package com.mystudycanada.shreehari.Model;

public class ExamModel {
    private String testName;
    private String date;
    private String time;
    private String id;
    private String isApplyExam;

    public void setTestName(String testName) {
        this.testName = testName;
    }

    public String getTestName() {
        return testName;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDate() {
        return date;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getTime() {
        return time;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setIsApplyExam(String isApplyExam) {
        this.isApplyExam = isApplyExam;
    }

    public String getIsApplyExam() {
        return isApplyExam;
    }
}
