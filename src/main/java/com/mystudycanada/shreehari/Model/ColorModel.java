package com.mystudycanada.shreehari.Model;

public class ColorModel {

    String type;
    String color;
    private String isApplyExam;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public void setIsApplyExam(String isApplyExam) {
        this.isApplyExam = isApplyExam;
    }

    public String getIsApplyExam() {
        return isApplyExam;
    }
}
