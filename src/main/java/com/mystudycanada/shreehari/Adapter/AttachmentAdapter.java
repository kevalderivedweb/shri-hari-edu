package com.mystudycanada.shreehari.Adapter;

import android.annotation.SuppressLint;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.mystudycanada.shreehari.Model.AttachmentModel;
import com.mystudycanada.shreehari.R;

import java.util.ArrayList;

public class AttachmentAdapter extends RecyclerView.Adapter<AttachmentAdapter.MyViewHolder> {


    private final OnItemClickListener listener;
    private ArrayList<AttachmentModel> mDataset;

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class MyViewHolder extends RecyclerView.ViewHolder {

        TextView name;
        ImageView download;
        ImageView demo_img;

        // each data item is just a string in this case


        public MyViewHolder(View v) {
            super(v);

            this.name = (TextView) itemView.findViewById(R.id.name);
            this.download = (ImageView) itemView.findViewById(R.id.download);
            this.demo_img = (ImageView) itemView.findViewById(R.id.demo_img);




        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public AttachmentAdapter(ArrayList<AttachmentModel> categoryModels, OnItemClickListener listener) {
        mDataset = categoryModels;
        this.listener = listener;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent,
                                           int viewType) {

        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        // create a new view
        View v = layoutInflater
                .inflate(R.layout.row_attechment, parent, false);
        MyViewHolder vh = new MyViewHolder(v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {

        Log.e("url",mDataset.get(position).getUrl());
       int finalInt = position + 1 ;
        holder.name.setText("Attachment  " + finalInt);
        holder.download.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onItemClick(position);
            }
        });
        holder.name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onItemClickWebView(position);
            }
        });
        holder.demo_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onItemClickWebView(position);
            }
        });
        Glide.with(holder.demo_img.getContext()).load(mDataset.get(position).getUrl()).into(holder.demo_img);


    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    public interface OnItemClickListener {
        void onItemClick(int item);
        void onItemClickWebView(int item);
    }


}
