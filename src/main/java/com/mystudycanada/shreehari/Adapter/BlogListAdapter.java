package com.mystudycanada.shreehari.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.mystudycanada.shreehari.Model.MoreInfoIeltsModel;
import com.mystudycanada.shreehari.R;

import java.util.ArrayList;

public class BlogListAdapter extends RecyclerView.Adapter<BlogListAdapter.ViewHolder> {

    private final OnItemClickListener listener;
    private Context context;
    private ArrayList<MoreInfoIeltsModel> infoIeltsModelArrayList;


    public BlogListAdapter(Context context, ArrayList<MoreInfoIeltsModel> infoIeltsModelArrayList,
                                    OnItemClickListener listener) {
        this.context = context;
        this.infoIeltsModelArrayList = infoIeltsModelArrayList;
        this.listener = listener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.blog_list_adapter, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        holder.blogListTitle.setText(infoIeltsModelArrayList.get(position).getTitle());

        holder.blogLetterFirst.setText(infoIeltsModelArrayList.get(position).getTitle().substring(0,1));

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                listener.onItemClick(infoIeltsModelArrayList.get(position).getId());
            }
        });

    }

    @Override
    public int getItemCount() {
        return infoIeltsModelArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView blogListTitle, blogLetterFirst;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            blogLetterFirst = itemView.findViewById(R.id.blogLetterFirst);
            blogListTitle = itemView.findViewById(R.id.blogListTitle);
        }
    }

    public interface OnItemClickListener {
        void onItemClick(String id);
    }

}
