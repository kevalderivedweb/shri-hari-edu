package com.mystudycanada.shreehari.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.mystudycanada.shreehari.Model.MoreInfoIeltsModel;
import com.mystudycanada.shreehari.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class MoreInfoIELTSListAdapter extends RecyclerView.Adapter<MoreInfoIELTSListAdapter.ViewHolder> {

    private final OnItemClickListener listener;
    private Context context;
    private ArrayList<MoreInfoIeltsModel> ieltsModelArrayList;


    public MoreInfoIELTSListAdapter(Context context, ArrayList<MoreInfoIeltsModel> ieltsModelArrayList,
                                    OnItemClickListener listener) {
        this.context = context;
        this.ieltsModelArrayList = ieltsModelArrayList;
        this.listener = listener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.more_info_adapter, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        holder.textInfoList.setText(ieltsModelArrayList.get(position).getTitle());

        Glide.with(context).load(ieltsModelArrayList.get(position).getImage()).into(holder.infoImageList);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                listener.onItemClick(ieltsModelArrayList.get(position).getId(), ieltsModelArrayList.get(position).getTitle());
            }
        });

    }

    @Override
    public int getItemCount() {
        return ieltsModelArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        ImageView infoImageList;
        TextView textInfoList;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            infoImageList = itemView.findViewById(R.id.infoImageList);
            textInfoList = itemView.findViewById(R.id.textInfoList);
        }
    }

    public interface OnItemClickListener {
        void onItemClick(String id, String title);
    }

}
