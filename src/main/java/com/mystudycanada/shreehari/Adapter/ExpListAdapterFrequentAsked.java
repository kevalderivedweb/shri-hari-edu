package com.mystudycanada.shreehari.Adapter;

import android.content.Context;
import android.os.Build;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.mystudycanada.shreehari.R;

import java.util.ArrayList;

public class ExpListAdapterFrequentAsked extends BaseExpandableListAdapter {

    private Context _context;

    private ArrayList<String> questionsArrayList;
    private ArrayList<String> answersArrayList;

    public ExpListAdapterFrequentAsked(Context context, ArrayList<String> questionsArrayList, ArrayList<String> answersArrayList) {
        this._context = context;
        this.questionsArrayList = questionsArrayList;
        this.answersArrayList = answersArrayList;

    }

    @Override
    public Object getChild(int groupPosition, int childPosititon) {
        return questionsArrayList.size();
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return groupPosition;
    }

    @Override
    public View getChildView(int groupPosition, final int childPosition,
                             boolean isLastChild, View convertView, ViewGroup parent) {


        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.list_list_newuser_answer, null);
        }

        TextView askedFrequentListExp = (TextView) convertView.findViewById(R.id.askedFrequentListExp);
        View tb_layout = (View) convertView.findViewById(R.id.tb_layout);



        askedFrequentListExp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                askedFrequentListExp.setClickable(false);
            }
        });

        askedFrequentListExp.setText(Html.fromHtml(answersArrayList.get(groupPosition)));


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            askedFrequentListExp.setText(Html.fromHtml(answersArrayList.get(groupPosition), Html.FROM_HTML_MODE_COMPACT));
        } else {
            askedFrequentListExp.setText(Html.fromHtml(answersArrayList.get(groupPosition)));
        }

        if(answersArrayList.get(groupPosition).isEmpty()){
            tb_layout.setVisibility(View.VISIBLE);
        }else {
            tb_layout.setVisibility(View.GONE);

        }


        Log.e("expList", answersArrayList.get(0) + " 111");
        return convertView;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return 1;
    }

    @Override
    public Object getGroup(int groupPosition) {
        return this.questionsArrayList.size();
    }

    @Override
    public int getGroupCount() {
        return questionsArrayList.size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded,
                             View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.list_group_newuser_question, null);
        }

        ImageView ivGroupIndicator = convertView.findViewById(R.id.ivJobsIndicator);

        TextView txtListGroupFrequent = (TextView) convertView.findViewById(R.id.txtListGroupFrequent);

        txtListGroupFrequent.setText( questionsArrayList.get(groupPosition));

        ivGroupIndicator.setSelected(isExpanded);

        Log.e("expgrooList", questionsArrayList.get(groupPosition) + " 111");

        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }
}
