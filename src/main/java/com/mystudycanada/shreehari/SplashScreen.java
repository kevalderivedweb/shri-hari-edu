package com.mystudycanada.shreehari;

import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.Window;
import android.widget.VideoView;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import com.mystudycanada.shreehari.UserSession.UserSession;

public class SplashScreen extends AppCompatActivity {

    private CountDownTimer countDownTimer;
    UserSession userSession;
    private VideoView mVideoView;

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
/*
        requestWindowFeature(Window.FEATURE_NO_TITLE);
*/
/*
        requestWindowFeature(Window.FEATURE_NO_TITLE);
*/
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_main);
        userSession = new UserSession(getApplicationContext());

        String uri = "android.resource://" + getPackageName() + "/" + R.raw.video_logo;
        mVideoView = (VideoView) findViewById(R.id.mVideoView);
        mVideoView.setVideoPath(uri);
//mVideoView.setVideoPath(uri);
        mVideoView.setVideoURI(Uri.parse(uri));
        mVideoView.start();
        getWindow().setStatusBarColor(getResources().getColor(R.color.colorPrimary));
        getWindow().setNavigationBarColor(getResources().getColor(R.color.colorPrimary));


        countDownTimer = new CountDownTimer(4500, 500) {

            public void onTick(long millisUntilFinished) {

            }

            public void onFinish() {
                if(userSession.isLoggedIn()){
                    Intent intent=new Intent(SplashScreen.this, MPin_Activity.class);
                    if(getIntent().getExtras()!=null) {
                        intent.putExtras(getIntent().getExtras());
                        setIntent(null);
                    }
                    startActivity(intent);
                    overridePendingTransition(R.anim.in_from_bottom, R.anim.in_from_top);
                    finish();
                }else {
                    Intent intent=new Intent(SplashScreen.this, Login_Activity.class);
                    if(getIntent().getExtras()!=null) {
                        intent.putExtras(getIntent().getExtras());
                        setIntent(null);
                    }
                    startActivity(intent);
                    overridePendingTransition(R.anim.in_from_bottom, R.anim.in_from_top);
                    finish();
                }



            }
        }.start();

    }
}