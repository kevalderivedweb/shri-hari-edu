package com.mystudycanada.shreehari;

import android.Manifest;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.DialogFragment;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.mystudycanada.shreehari.API.GetExamCenterRequest;
import com.mystudycanada.shreehari.API.GetStandardRequest;
import com.mystudycanada.shreehari.API.ServerUtils;
import com.mystudycanada.shreehari.API.VolleyMultipartRequest;
import com.mystudycanada.shreehari.Adapter.SpinAdapter2;
import com.mystudycanada.shreehari.UserSession.UserSession;
import com.mystudycanada.shreehari.ui.Attendance;
import com.mystudycanada.shreehari.ui.StandardModel;
import com.sdsmdg.tastytoast.TastyToast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;

public class IELTSExamBook extends AppCompatActivity {


    private String mDate;
    private String mType;
    private RadioGroup exam_type;
    private RadioGroup exam_mode;
    private String mExamType = "Academic";
    private String mExamMode = "IDP";
    private String mExamWay = "Pen-Paper";
    private TextView exam_date;
    private UserSession mUserSession;
    private TextView full_name;
    private final int PICK_IMAGE_CAMERA = 1, PICK_IMAGE_GALLERY = 2;
    private Bitmap bitmap;
    private String imgPath = null;
    private File destination = null;
    private InputStream inputStreamImg;
    private TextView mobile;
    private Spinner exam_center;
    private RequestQueue requestQueue;
    private ArrayList<StandardModel> mDataset = new ArrayList<>();
    private ImageView upload_result;
    private TextView email;
    private RadioGroup exam_way;
    private EditText passport_number;
    private int exam_center_pos=-1;
    private static TextView dob;
    String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_exam_book);

        findViewById(R.id.back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        mUserSession = new UserSession(this);
        requestQueue = Volley.newRequestQueue(IELTSExamBook.this);//Creating the RequestQueue

        getWindow().setStatusBarColor(getResources().getColor(R.color.colorPrimary));
        getWindow().setNavigationBarColor(getResources().getColor(R.color.colorPrimary));



        upload_result = (ImageView) findViewById(R.id.upload_result);
        exam_center = findViewById(R.id.exam_center);
        Intent intent = getIntent();
        Bundle extras = intent.getExtras();
        if (extras != null) {
            mDate = extras.getString("date"); // replace
            mType = extras.getString("type"); // replace
        }

        SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat sdf2 = new SimpleDateFormat("dd-MM-yyyy");

        try {

            Date date = sdf1.parse(mDate);
            mDate = sdf2.format(date);

        } catch (ParseException e) {
            e.printStackTrace();
        }


        exam_type = findViewById(R.id.exam_type);
        exam_type.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                switch (i) {
                    case R.id.academic:
                        mExamType = "Academic";
                        break;
                    case R.id.general:
                        mExamType = "General";
                        break;
                }
            }
        });
        exam_mode = findViewById(R.id.exam_mode);
        exam_mode.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                switch (i) {
                    case R.id.idp:
                        mExamMode = "IDP";
                        break;
                    case R.id.british:
                        mExamMode = "British";
                        break;
                        case R.id.cambridge:
                        mExamMode = "Cambridge";
                        break;
                }
            }
        });
        exam_way = findViewById(R.id.exam_way);
        exam_way.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                switch (i) {
                    case R.id.pen_paper:
                        mExamWay = "Pen-Paper";
                        break;
                    case R.id.computer:
                        mExamWay = "Computer Delivered";
                        break;

                }
            }
        });
        if(mType.equals("Academic")) {
            exam_type.check(R.id.academic);
        }else {
            exam_type.check(R.id.general);
        }
        /*exam_type = findViewById(R.id.exam_type);
        exam_type.setText(mType);*/
        dob = findViewById(R.id.dob);
        passport_number = findViewById(R.id.passport_number);
        exam_date = findViewById(R.id.exam_date);
        exam_date.setText(mDate);
        mobile = findViewById(R.id.mobile);
        mobile.setText(mUserSession.getMobile());
        email = findViewById(R.id.email);
        email.setText(mUserSession.getEmail());

        full_name = findViewById(R.id.full_name);
        full_name.setText(mUserSession.getName() + " " + mUserSession.getLastName());

        findViewById(R.id.passport_pic_photo).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectImage();
            }
        });

        findViewById(R.id.submit).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

              /*  Log.e("ExamData",mUserSession.getInquiryId()+
                        "---"+mExamType+"---"+mExamMode+"---"+mDate+"---"+full_name.getText().toString()+"---"+mUserSession.getMobile()
                        +"---"+passport_number.getText().toString()+"---"+email.getText().toString()+"---"+mExamWay+"---"+mUserSession.getBranchId()+"---"+ mDataset.get(exam_center_pos).getCoaching_id()
                        );
                */
                if(full_name.getText().toString().isEmpty()){
                    Toast.makeText(IELTSExamBook.this,"Please enter your Full Name",Toast.LENGTH_LONG).show();
                }else if(passport_number.getText().toString().isEmpty()){
                    Toast.makeText(IELTSExamBook.this,"Please enter your Passport Number",Toast.LENGTH_LONG).show();
                }else if(email.getText().toString().isEmpty() ){
                    Toast.makeText(IELTSExamBook.this,"Please enter your Email",Toast.LENGTH_LONG).show();
                }else if (!email.getText().toString().trim().matches(emailPattern)){
                    Toast.makeText(getApplicationContext(),"Invalid email address",Toast.LENGTH_SHORT).show();
                }else if(exam_center_pos==-1){
                    Toast.makeText(IELTSExamBook.this,"Please Select Exam Center",Toast.LENGTH_LONG).show();
                }else if(bitmap==null){
                    Toast.makeText(IELTSExamBook.this,"Please Select Passport File",Toast.LENGTH_LONG).show();
                }else if(dob.getText().toString().equals("Please select date")){
                    Toast.makeText(IELTSExamBook.this,"Please Select Your Birth Date",Toast.LENGTH_LONG).show();
                }else {
                  //  Log.e("ExamData",mDate+"---"+dob.getText().toString());
                    UploadDatatoServer();
                }
            }
        });

        dob.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogFragment newFragment = new DatePickerFragment();
                newFragment.show(getSupportFragmentManager(), "datePicker");
            }
        });

        exam_center.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                exam_center_pos = i;

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        GetExamCenter();
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    private void GetExamCenter() {
        final KProgressHUD progressDialog = KProgressHUD.create(IELTSExamBook.this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("Please wait")
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();

        GetExamCenterRequest loginRequest1 = new GetExamCenterRequest(new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("Response", response + " null");
                progressDialog.dismiss();
                mDataset.clear();
                JSONObject jsonObject = null;
                try {
                    jsonObject = new JSONObject(response);

                    JSONArray jsonArray = jsonObject.getJSONArray("data");

                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject object = jsonArray.getJSONObject(i);
                        StandardModel BatchModel = new StandardModel();
                        BatchModel.setCoaching_id(object.getString("exam_center_id"));
                        BatchModel.setCoaching(object.getString("exam_center"));
                        BatchModel.setStatus(object.getString("status"));
                        mDataset.add(BatchModel);
                    }

                    StandardModel BatchModel = new StandardModel();
                    BatchModel.setCoaching_id("");
                    BatchModel.setCoaching("Please select Exam Center");
                    BatchModel.setStatus("");
                    mDataset.add(BatchModel);
                    SpinAdapter2 adapter = new SpinAdapter2(IELTSExamBook.this,
                            android.R.layout.simple_spinner_item,
                            mDataset);
                    adapter.setDropDownViewResource(R.layout.spinner_item);
                    exam_center.setAdapter(adapter);
                    exam_center.setSelection(adapter.getCount());

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                progressDialog.dismiss();
                if (error instanceof ServerError)
                    Toast.makeText(IELTSExamBook.this, "Server Error", Toast.LENGTH_LONG).show();
                else if (error instanceof TimeoutError)
                    Toast.makeText(IELTSExamBook.this, "Connection Timed Out", Toast.LENGTH_LONG).show();
                else if (error instanceof NetworkError)
                    Toast.makeText(IELTSExamBook.this, "Bad Network Connection", Toast.LENGTH_LONG).show();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                // params.put("Accept", "application/json");
                params.put("Authorization", "Bearer " + mUserSession.getAPIToken());
                return params;
            }
        };
        loginRequest1.setTag("TAG");
        loginRequest1.setShouldCache(false);
        loginRequest1.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 60000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 60000;
            }

            @Override
            public void retry(VolleyError error) throws VolleyError {

            }
        });
        requestQueue.add(loginRequest1);
    }


    // Select image from camera and gallery
    private void selectImage() {
        try {
            PackageManager pm = IELTSExamBook.this.getPackageManager();
            int hasPerm = pm.checkPermission(Manifest.permission.CAMERA, IELTSExamBook.this.getPackageName());
            if (hasPerm == PackageManager.PERMISSION_GRANTED) {
                final CharSequence[] options = {getString(R.string.take_photo), getString(R.string.gallary), getString(R.string.cancel)};
                AlertDialog.Builder builder = new AlertDialog.Builder(IELTSExamBook.this);
                builder.setTitle(R.string.select_option);
                builder.setItems(options, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int item) {
                        if (options[item].equals(getResources().getString(R.string.take_photo))) {
                            dialog.dismiss();
                            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                            startActivityForResult(intent, PICK_IMAGE_CAMERA);
                        } else if (options[item].equals(getResources().getString(R.string.gallary))) {
                            dialog.dismiss();
                            Intent pickPhoto = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                            startActivityForResult(pickPhoto, PICK_IMAGE_GALLERY);
                        } else if (options[item].equals(getResources().getString(R.string.cancel))) {
                            dialog.dismiss();
                        }
                    }
                });
                builder.show();
            } else
                checkAndroidVersion();
            //Toast.makeText(IELTSExamBook.this, "Camera Permission error", Toast.LENGTH_LONG).show();
        } catch (Exception e) {
            checkAndroidVersion();
            //Toast.makeText(IELTSExamBook.this, "Camera Permission error", Toast.LENGTH_LONG).show();
            e.printStackTrace();
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        inputStreamImg = null;
        if (requestCode == PICK_IMAGE_CAMERA) {
            try {
                Uri selectedImage = data.getData();
                bitmap = (Bitmap) data.getExtras().get("data");
                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 50, bytes);

                Log.e("Activity", "Pick from Camera::>>> ");

                File folder = new File(Environment.getExternalStorageDirectory() +
                        File.separator + "BonCopBadCop2");
                if (!folder.exists()) {
                    folder.mkdirs();
                }
                String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(new Date());
                destination = new File(Environment.getExternalStorageDirectory() + "/" +
                        "BonCopBadCop2", "IMG_" + timeStamp + ".jpg");
                FileOutputStream fo;
                try {
                    destination.createNewFile();
                    fo = new FileOutputStream(destination);
                    fo.write(bytes.toByteArray());
                    fo.close();
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                imgPath = destination.getAbsolutePath();
                Glide.with(Objects.requireNonNull(IELTSExamBook.this)).asBitmap().load(bitmap).circleCrop().into(upload_result);
                //txt_injury.setText(imgPath);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (requestCode == PICK_IMAGE_GALLERY) {
            Uri selectedImage = data.getData();
            try {
                bitmap = MediaStore.Images.Media.getBitmap(IELTSExamBook.this.getContentResolver(), selectedImage);
                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 50, bytes);
                Log.e("Activity", "Pick from Gallery::>>> ");

                imgPath = getRealPathFromURI(selectedImage);
                destination = new File(imgPath.toString());
                //  txt_injury.setText(imgPath);
                Glide.with(Objects.requireNonNull(IELTSExamBook.this)).asBitmap().load(bitmap).circleCrop().into(upload_result);

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public String getRealPathFromURI(Uri contentUri) {
        String[] proj = {MediaStore.Audio.Media.DATA};
        Cursor cursor = IELTSExamBook.this.managedQuery(contentUri, proj, null, null, null);
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Audio.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }

    private void checkAndroidVersion() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            checkAndRequestPermissions();

        } else {
            // code for lollipop and pre-lollipop devices
        }

    }

    private boolean checkAndRequestPermissions() {
        int camera = ContextCompat.checkSelfPermission(IELTSExamBook.this,
                Manifest.permission.CAMERA);
        int wtite = ContextCompat.checkSelfPermission(IELTSExamBook.this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        int read = ContextCompat.checkSelfPermission(IELTSExamBook.this, Manifest.permission.READ_EXTERNAL_STORAGE);
        List<String> listPermissionsNeeded = new ArrayList<>();
        if (wtite != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }
        if (camera != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.CAMERA);
        }
        if (read != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.READ_EXTERNAL_STORAGE);
        }
        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(IELTSExamBook.this, listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), REQUEST_ID_MULTIPLE_PERMISSIONS);
            return false;
        }
        return true;
    }

    public static final int REQUEST_ID_MULTIPLE_PERMISSIONS = 7;

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        Log.d("in fragment on request", "Permission callback called-------");
        switch (requestCode) {
            case REQUEST_ID_MULTIPLE_PERMISSIONS: {

                Map<String, Integer> perms = new HashMap<>();
                // Initialize the map with both permissions
                perms.put(Manifest.permission.WRITE_EXTERNAL_STORAGE, PackageManager.PERMISSION_GRANTED);
                perms.put(Manifest.permission.CAMERA, PackageManager.PERMISSION_GRANTED);
                perms.put(Manifest.permission.READ_EXTERNAL_STORAGE, PackageManager.PERMISSION_GRANTED);
                // Fill with actual results from user
                if (grantResults.length > 0) {
                    for (int i = 0; i < permissions.length; i++)
                        perms.put(permissions[i], grantResults[i]);
                    // Check for both permissions
                    if (perms.get(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                            && perms.get(Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED && perms.get(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                        Log.d("in fragment on request", "CAMERA & WRITE_EXTERNAL_STORAGE READ_EXTERNAL_STORAGE permission granted");
                        // process the normal flow
                        //else any one or both the permissions are not granted
                    } else {
                        Log.d("in fragment on request", "Some permissions are not granted ask again ");
                        //permission is denied (this is the first time, when "never ask again" is not checked) so ask again explaining the usage of permission
//                        // shouldShowRequestPermissionRationale will return true
                        //show the dialog or snackbar saying its necessary and try again otherwise proceed with setup.
                        if (ActivityCompat.shouldShowRequestPermissionRationale(IELTSExamBook.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) || ActivityCompat.shouldShowRequestPermissionRationale(IELTSExamBook.this, Manifest.permission.CAMERA) || ActivityCompat.shouldShowRequestPermissionRationale(IELTSExamBook.this, Manifest.permission.READ_EXTERNAL_STORAGE)) {
                            showDialogOK("Camera and Storage Permission required for this app",
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            switch (which) {
                                                case DialogInterface.BUTTON_POSITIVE:
                                                    checkAndRequestPermissions();
                                                    break;
                                                case DialogInterface.BUTTON_NEGATIVE:
                                                    // proceed with logic by disabling the related features or quit the app.
                                                    break;
                                            }
                                        }
                                    });
                        }
                        //permission is denied (and never ask again is  checked)
                        //shouldShowRequestPermissionRationale will return false
                        else {
                            Toast.makeText(IELTSExamBook.this, "Go to settings and enable permissions", Toast.LENGTH_LONG)
                                    .show();
                            //                            //proceed with logic by disabling the related features or quit the app.
                        }
                    }
                }
            }
        }
    }
    private void showDialogOK(String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(IELTSExamBook.this)
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", okListener)
                .create()
                .show();
    }

    private void UploadDatatoServer() {
        final KProgressHUD progressDialog = KProgressHUD.create(IELTSExamBook.this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("Please wait")
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();

        VolleyMultipartRequest volleyMultipartRequest = new VolleyMultipartRequest(Request.Method.POST, ServerUtils.BASE_URL + "apply-ielts-exam",
                new Response.Listener<NetworkResponse>() {
                    @Override
                    public void onResponse(NetworkResponse response) {

                        progressDialog.dismiss();

                        try {

                            JSONObject jsonObject = new JSONObject(new String(response.data));
                            Log.e("Response",jsonObject.toString() + " --");
                            if (jsonObject.getString("ResponseCode").equals("200")){
                                try {
                                    TastyToast.makeText(getApplicationContext(), jsonObject.getString("ResponseMsg"), TastyToast.LENGTH_LONG, TastyToast.SUCCESS);

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                                finish();

                            }else {
                                TastyToast.makeText(getApplicationContext(), jsonObject.getString("ResponseMsg"), TastyToast.LENGTH_LONG, TastyToast.WARNING);

                            }

                        } catch (Exception e) {
                            TastyToast.makeText(getApplicationContext(),e.getMessage(), TastyToast.LENGTH_LONG, TastyToast.WARNING);

                           /* session.logout();
                            Intent intent = new Intent(Activity_ManageMyWishList.this, Activity_SelectCity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                            finish();*/
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                      public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        try {
                            String responseBody = new String(error.networkResponse.data, "utf-8");
                            JSONObject data = new JSONObject(responseBody);
                           // JSONArray errors = data.getJSONArray("errors");
                           // JSONObject jsonMessage = data.getJSONObject(0);

                            Log.e("ErrorResponse",data.toString()+"");
                            String message = data.getString("message");
                            Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
                        } catch (JSONException e) {
                        } catch (UnsupportedEncodingException errorr) {
                        } }
                }) {


            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("inquiry_id", mUserSession.getInquiryId());
                params.put("exam_type", mExamType);
                params.put("exam_mode", mExamMode);
                params.put("exam_date", mDate);
                params.put("student_fullname", full_name.getText().toString());
                params.put("student_mobile", mUserSession.getMobile());
                params.put("student_dob", dob.getText().toString());
                params.put("student_passport",passport_number.getText().toString());
                params.put("student_email",email.getText().toString());
                params.put("exam_center", mDataset.get(exam_center_pos).getCoaching_id());
                params.put("exam_way", mExamWay);
                params.put("branch_id", mUserSession.getBranchId());

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Accept", "application/json");
                params.put("Authorization", "Bearer " + mUserSession.getAPIToken());
                return params;
            }

            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();
                long imagename = System.currentTimeMillis();
                    params.put("student_passport_file", new DataPart(imagename + ".png", getFileDataFromDrawable(bitmap)));
                return params;
            }
        };
        //adding the request to volley
        Volley.newRequestQueue(IELTSExamBook.this).add(volleyMultipartRequest);
    }

    public byte[] getFileDataFromDrawable(Bitmap bitmap) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 80, byteArrayOutputStream);
        return byteArrayOutputStream.toByteArray();

    }

    public static class DatePickerFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener {
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            final Calendar c = Calendar.getInstance();
            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH);
            int day = c.get(Calendar.DAY_OF_MONTH);
            DatePickerDialog dialog = new DatePickerDialog(getActivity(), this, year, month, day);
            dialog.getDatePicker().setMaxDate(c.getTimeInMillis());
            return dialog;
        }

        public void onDateSet(DatePicker view, int year, int month, int day) {
            Calendar calendar = Calendar.getInstance();
            calendar.set(year, month, day);
            SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
            String dateString = dateFormat.format(calendar.getTime());
            dob.setText(dateString);
            //GetAttandance(formattedDate,mDataset1.get(batch_pos).getBatch_id(),mDataset2.get(standard_pos).getCoaching_id());
        }
    }
}