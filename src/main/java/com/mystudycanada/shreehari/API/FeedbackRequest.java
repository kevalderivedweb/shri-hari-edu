package com.mystudycanada.shreehari.API;

import com.android.volley.AuthFailureError;
import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by kshitij on 12/17/17.
 */

public class FeedbackRequest extends StringRequest {

    private Map<String, String> parameters;

    public FeedbackRequest(String rating,String comment, Response.Listener<String> listener, Response.ErrorListener errorListener) {
        super(Method.POST, ServerUtils.BASE_URL+"give-feedback", listener, errorListener);
        parameters = new HashMap<>();
        parameters.put("rating", rating);
        parameters.put("comment",comment);

    }


    @Override
    protected Map<String, String> getParams() throws AuthFailureError {
        return parameters;
    }

}
