package com.mystudycanada.shreehari.API;

import com.android.volley.AuthFailureError;
import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;

import java.util.HashMap;
import java.util.Map;


public class GetDemoLecture extends StringRequest {

    private Map<String, String> parameters;

    public GetDemoLecture(String batchId, String date, Response.Listener<String> listener, Response.ErrorListener errorListener) {
        super(Method.POST, ServerUtils.BASE_URL+"book-demo-lecture", listener, errorListener);
        parameters = new HashMap<>();
        parameters.put("batch_id", batchId);
        parameters.put("demo_schedule_date", date);

    }

    @Override
    protected Map<String, String> getParams() throws AuthFailureError {
        return parameters;
    }

}