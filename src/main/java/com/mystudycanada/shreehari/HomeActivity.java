package com.mystudycanada.shreehari;

import android.Manifest;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.ActivityManager;
import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.IdRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.app.NotificationManagerCompat;
import androidx.core.content.ContextCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.navigation.ui.AppBarConfiguration;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.mystudycanada.shreehari.API.GetBatchRequest;
import com.mystudycanada.shreehari.API.GetCoachingLevelRequest;
import com.mystudycanada.shreehari.API.GetDemoLecture;
import com.mystudycanada.shreehari.API.GetRegisterWithUs;
import com.mystudycanada.shreehari.API.GetStandardRequest;
import com.mystudycanada.shreehari.API.ServerUtils;
import com.mystudycanada.shreehari.API.VolleyMultipartRequest;
import com.mystudycanada.shreehari.Adapter.CoatchingLevelAdapter;
import com.mystudycanada.shreehari.Adapter.SpinAdapter;
import com.mystudycanada.shreehari.Adapter.SpinAdapter2;
import com.mystudycanada.shreehari.Model.BatchModel;
import com.mystudycanada.shreehari.Model.CoachingLevelModel;
import com.mystudycanada.shreehari.Model.SubjectModel;
import com.mystudycanada.shreehari.UserSession.UserSession;
import com.mystudycanada.shreehari.ui.AddAssignment;
import com.mystudycanada.shreehari.ui.AddSchedule;
import com.mystudycanada.shreehari.ui.Announcements;
import com.mystudycanada.shreehari.ui.Assignment;
import com.mystudycanada.shreehari.ui.Attendance;
import com.mystudycanada.shreehari.ui.Connect;
import com.mystudycanada.shreehari.ui.Dashboard;
import com.mystudycanada.shreehari.ui.DashboardNewUser;
import com.mystudycanada.shreehari.ui.DownloadImages;
import com.mystudycanada.shreehari.ui.EXTRASCREEN;
import com.mystudycanada.shreehari.ui.Exam;
import com.mystudycanada.shreehari.ui.Gallery;
import com.mystudycanada.shreehari.ui.GetDemoBatchRequest;
import com.mystudycanada.shreehari.ui.ILTSEXAM;
import com.mystudycanada.shreehari.ui.Leave;
import com.mystudycanada.shreehari.ui.Notification;
import com.mystudycanada.shreehari.ui.Profile;
import com.mystudycanada.shreehari.ui.Result;
import com.mystudycanada.shreehari.ui.ResultExamName;
import com.mystudycanada.shreehari.ui.Schedule;
import com.mystudycanada.shreehari.ui.StandardModel;
import com.mystudycanada.shreehari.ui.StudentAttendance;
import com.mystudycanada.shreehari.ui.StudentSearch;
import com.mystudycanada.shreehari.ui.StudentSetting;
import com.mystudycanada.shreehari.ui.Visa;
import com.google.android.material.navigation.NavigationView;
import com.sdsmdg.tastytoast.TastyToast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

public class HomeActivity extends AppCompatActivity implements Profile.OnItemClickListener {


    private static final int NOTIFICATION_ID = 112;
    private AppBarConfiguration mAppBarConfiguration;
    private ImageView drawer_menu;
    private LinearLayout ln_assignment, ln_assignment1, ln_schedule, ln_schedule_2, ln_announcements;
    private LinearLayout ln_attendance, ln_exam, ln_notification, ln_result;
    private LinearLayout ln_gallery;
    private LinearLayout ln_leave;
    private LinearLayout ln_chat;
    private LinearLayout ln_profile;
    private LinearLayout ln_student;
    private LinearLayout ln_visa;
    private LinearLayout ln_extra;
    private LinearLayout ln_dashboard;
    private LinearLayout ln_connect;
    private LinearLayout ln_setting;
    private View ln_setting_view;
    private TextView NAME1, NAME2, NAME3, NAME4;
    private TextView reg_no;
    private TextView regdate;
    private View ln_profile_view;
    private View ln_student_view;
    private LinearLayout ln_logout;
    private LinearLayout ln_ilts;
    private NotificationManagerCompat mNotificationManager;
    private String Type;
    private TextView txt_setting;

    public static boolean IsProfileOpen = false;
    private static final int CAMERA_PERMISSION_CODE = 100;
    private static final int STORAGE_PERMISSION_CODE = 101;
    private LinearLayout ln_downloads;

    private ImageView arr2, arr3, arr4, arr5, arr6, arr7, arr8, arr9, arr10, arr11, arr12, arr13, arr013;


    FirebaseUser firebaseUser;
    DatabaseReference reference;
    private UserSession userSession;


    private ArrayList<StandardModel> mDataset2 = new ArrayList<>();
    private ArrayList<BatchModel> mDataset = new ArrayList<>();
    private ArrayList<CoachingLevelModel> mDataset4 = new ArrayList<>();
    private ArrayList<SubjectModel> mDataset3 = new ArrayList<>();

    private Spinner standard, coaching_level, batch;
    private int standard_pos = 0, coachin_pos = 0;

    private String batchId = "";
    private static TextView dateText;
    private Dialog dialogForCategories;

    private RequestQueue requestQueue;
    private ImageView profile_image1;

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        requestQueue = Volley.newRequestQueue(HomeActivity.this);//Creating the RequestQueue
        getWindow().setStatusBarColor(getResources().getColor(R.color.colorPrimary));
        getWindow().setNavigationBarColor(getResources().getColor(R.color.colorPrimary));

        userSession = new UserSession(getApplicationContext());
        firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        reference = FirebaseDatabase.getInstance().getReference(userSession.getBranchId()).child("Users").child(firebaseUser.getUid());
        arr2 = findViewById(R.id.arr2);
        arr3 = findViewById(R.id.arr3);
        arr4 = findViewById(R.id.arr4);
        arr5 = findViewById(R.id.arr5);
        arr6 = findViewById(R.id.arr6);
        arr7 = findViewById(R.id.arr7);
        arr8 = findViewById(R.id.arr8);
        arr9 = findViewById(R.id.arr9);
        arr10 = findViewById(R.id.arr10);
        arr11 = findViewById(R.id.arr11);
        arr12 = findViewById(R.id.arr12);
        arr13 = findViewById(R.id.arr13);
        arr013 = findViewById(R.id.arr013);

        if (!userSession.getRegisterUser()) {
            arr2.setImageResource(R.drawable.padlock);
            arr3.setImageResource(R.drawable.padlock);
            arr4.setImageResource(R.drawable.padlock);
            arr5.setImageResource(R.drawable.padlock);
            arr6.setImageResource(R.drawable.padlock);
            arr7.setImageResource(R.drawable.padlock);
            arr8.setImageResource(R.drawable.padlock);
            arr9.setImageResource(R.drawable.padlock);
            arr10.setImageResource(R.drawable.padlock);
            arr11.setImageResource(R.drawable.padlock);
            arr12.setImageResource(R.drawable.padlock);
            arr13.setImageResource(R.drawable.padlock);
            arr013.setImageResource(R.drawable.padlock);
        }




        checkPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE, STORAGE_PERMISSION_CODE);

        drawer_menu = findViewById(R.id.menu);
        NAME1 = findViewById(R.id.name1);
        NAME2 = findViewById(R.id.name2);
        NAME3 = findViewById(R.id.name3);
        NAME4 = findViewById(R.id.name_ds);
        reg_no = findViewById(R.id.reg_no);
        regdate = findViewById(R.id.regdate);
        ln_assignment = findViewById(R.id.ln_assignment);
        profile_image1 = findViewById(R.id.profile_image1);
        profile_image2 = findViewById(R.id.profile_image2);
        ln_dashboard = findViewById(R.id.ln_dashboard);
        ln_visa = findViewById(R.id.ln_visa);
        ln_student = findViewById(R.id.ln_student);
        ln_result = findViewById(R.id.ln_result);
        ln_notification = findViewById(R.id.ln_notification);
        ln_schedule = findViewById(R.id.ln_schedule);
        ln_schedule_2 = findViewById(R.id.ln_schedule2);
        ln_announcements = findViewById(R.id.ln_announcements);
        ln_profile = findViewById(R.id.ln_profile);
        ln_downloads = findViewById(R.id.ln_downloads);
        ln_profile_view = findViewById(R.id.ln_profile_view);
        ln_student_view = findViewById(R.id.ln_student_view);
        ln_attendance = findViewById(R.id.ln_attendance);
        ln_assignment1 = findViewById(R.id.ln_assignment1);
        ln_gallery = findViewById(R.id.ln_gallery);
        ln_extra = findViewById(R.id.ln_extra);
        ln_leave = findViewById(R.id.ln_leave);
        ln_chat = findViewById(R.id.ln_chat);
        ln_exam = findViewById(R.id.ln_exam);
        ln_connect = findViewById(R.id.ln_connect);
        ln_setting = findViewById(R.id.ln_setting);
        ln_ilts = findViewById(R.id.ln_ilts);
        ln_logout = findViewById(R.id.ln_logout);
        ln_setting_view = findViewById(R.id.ln_setting_view);
        txt_setting = findViewById(R.id.txt_setting);
        final DrawerLayout drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        drawer_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                drawer.openDrawer(Gravity.LEFT);
            }
        });


        //userSession.setRegisterUser(false);

        Log.e("Type", ServerUtils.TYPE);

        if (ServerUtils.TYPE.equalsIgnoreCase("Announcement")) {
            ServerUtils.TYPE = "";
            replaceFragment(R.id.nav_host_fragment, new Announcements(), "Fragment", null);
        } else if (ServerUtils.TYPE.equalsIgnoreCase("Schedule")) {
            ServerUtils.TYPE = "";
            replaceFragment(R.id.nav_host_fragment, new Schedule(), "Fragment", null);
        } else if (ServerUtils.TYPE.equalsIgnoreCase("Result")) {
            ServerUtils.TYPE = "";
            if (userSession.getUserType().equalsIgnoreCase("admin")) {
                replaceFragment(R.id.nav_host_fragment, new Result(), "Fragment", null);
            } else if (userSession.getUserType().equalsIgnoreCase("parent")) {
                ResultExamName fragobj = new ResultExamName();
                Bundle bundle = new Bundle();
                bundle.putString("Id", userSession.getUserId());
                fragobj.setArguments(bundle);
                replaceFragment(R.id.nav_host_fragment, fragobj, "Fragment", null);
            } else {
                ResultExamName fragobj = new ResultExamName();
                Bundle bundle = new Bundle();
                bundle.putString("Id", userSession.getUserId());
                fragobj.setArguments(bundle);
                replaceFragment(R.id.nav_host_fragment, fragobj, "Fragment", null);
            }
        } else if (ServerUtils.TYPE.equalsIgnoreCase("Attendance")) {
            ServerUtils.TYPE = "";
            if (userSession.getUserType().equalsIgnoreCase("admin")) {
                replaceFragment(R.id.nav_host_fragment, new Attendance(), "Fragment", null);
            } else {
                replaceFragment(R.id.nav_host_fragment, new StudentAttendance(), "Fragment", null);
            }
        } else if (ServerUtils.TYPE.equalsIgnoreCase("Exam")) {
            ServerUtils.TYPE = "";
            replaceFragment(R.id.nav_host_fragment, new Exam(), "Fragment", null);
        } else if (ServerUtils.TYPE.equalsIgnoreCase("Leave")) {
            ServerUtils.TYPE = "";
            replaceFragment(R.id.nav_host_fragment, new Leave(), "Fragment", null);
        } else {
            if (userSession.getRegisterUser()) {
                replaceFragment(R.id.nav_host_fragment, new Dashboard(), "Fragment", null);
            } else {
                replaceFragment(R.id.nav_host_fragment, new DashboardNewUser(), "FragmentNewUser", null);
            }
        }

        ln_dashboard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                HomeActivity.IsProfileOpen = false;
                if (userSession.getRegisterUser()) {
                    replaceFragment(R.id.nav_host_fragment, new Dashboard(), "Fragment", null);
                } else {
                    replaceFragment(R.id.nav_host_fragment, new DashboardNewUser(), "FragmentNewUser", null);
                }
                drawer.closeDrawer(Gravity.LEFT);

            }
        });
        Log.e("token", userSession.getFirebaseToken());

        ln_connect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                HomeActivity.IsProfileOpen = false;
                replaceFragment(R.id.nav_host_fragment, new Connect(), "Fragment", null);
                drawer.closeDrawer(Gravity.LEFT);

            }
        });

        ln_assignment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                HomeActivity.IsProfileOpen = false;
                replaceFragment(R.id.nav_host_fragment, new AddAssignment(), "Fragment", null);
                drawer.closeDrawer(Gravity.LEFT);
            }
        });

        ln_assignment1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                HomeActivity.IsProfileOpen = false;
                replaceFragment(R.id.nav_host_fragment, new Assignment(), "Fragment", null);
                drawer.closeDrawer(Gravity.LEFT);
            }
        });


        ln_gallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                HomeActivity.IsProfileOpen = false;
                if (userSession.getRegisterUser()) {
                    replaceFragment(R.id.nav_host_fragment, new Gallery(), "Fragment", null);
                } else {
                    TastyToast.makeText(getApplicationContext(), "Please Register with us to access this Feature", TastyToast.LENGTH_LONG, TastyToast.ERROR);
                }
                drawer.closeDrawer(Gravity.LEFT);
            }
        });

        ln_ilts.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                HomeActivity.IsProfileOpen = false;
                if (userSession.getRegisterUser()) {
                    replaceFragment(R.id.nav_host_fragment, new ILTSEXAM(), "Fragment", null);
                } else {
                    TastyToast.makeText(getApplicationContext(), "Please Register with us to access this Feature", TastyToast.LENGTH_LONG, TastyToast.ERROR);
                }
                drawer.closeDrawer(Gravity.LEFT);
            }
        });

        ln_setting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                HomeActivity.IsProfileOpen = false;
                replaceFragment(R.id.nav_host_fragment, new StudentSetting(), "Fragment", null);
                drawer.closeDrawer(Gravity.LEFT);
            }
        });

        ln_result.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                HomeActivity.IsProfileOpen = false;
                if (userSession.getRegisterUser()) {
                    if (userSession.getUserType().equals("admin")) {
                        replaceFragment(R.id.nav_host_fragment, new Result(), "Fragment", null);
                    } else if (userSession.getUserType().equals("parent")) {
                        ResultExamName fragobj = new ResultExamName();
                        Bundle bundle = new Bundle();
                        bundle.putString("Id", userSession.getUserId());
                        fragobj.setArguments(bundle);
                        replaceFragment(R.id.nav_host_fragment, fragobj, "Fragment", null);

                    } else {
                        ResultExamName fragobj = new ResultExamName();
                        Bundle bundle = new Bundle();
                        bundle.putString("Id", userSession.getUserId());
                        fragobj.setArguments(bundle);
                        replaceFragment(R.id.nav_host_fragment, fragobj, "Fragment", null);

                    }
                } else {
                    TastyToast.makeText(getApplicationContext(), "Please Register with us to access this Feature", TastyToast.LENGTH_LONG, TastyToast.ERROR);
                }
                drawer.closeDrawer(Gravity.LEFT);

            }
        });

        ln_notification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                HomeActivity.IsProfileOpen = false;
                replaceFragment(R.id.nav_host_fragment, new Notification(), "Fragment", null);
                drawer.closeDrawer(Gravity.LEFT);
            }
        });

        ln_leave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                HomeActivity.IsProfileOpen = false;
                if (userSession.getRegisterUser()) {
                    replaceFragment(R.id.nav_host_fragment, new Leave(), "Fragment", null);
                } else {
                    TastyToast.makeText(getApplicationContext(), "Please Register with us to access this Feature", TastyToast.LENGTH_LONG, TastyToast.ERROR);
                }
                drawer.closeDrawer(Gravity.LEFT);
            }
        });

        ln_chat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                HomeActivity.IsProfileOpen = false;
                if (userSession.getRegisterUser()) {
                    replaceFragment(R.id.nav_host_fragment, new Connect(), "Fragment", null);
                } else {
                    TastyToast.makeText(getApplicationContext(), "Please Register with us to access this Feature", TastyToast.LENGTH_LONG, TastyToast.ERROR);
                }
                drawer.closeDrawer(Gravity.LEFT);
            }
        });

        ln_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                userSession.logout();
                clearAppData();
                restartApp();

           /*     Intent intent = new Intent(HomeActivity.this,Login_Activity.class);
                startActivity(intent);
                finish();*/
            }
        });

        if (userSession.getUserType().equals("parent")) {
            ln_setting.setVisibility(View.VISIBLE);
            ln_setting_view.setVisibility(View.VISIBLE);
        } else {
            ln_setting.setVisibility(View.GONE);
            ln_setting_view.setVisibility(View.GONE);
        }
        if (userSession.getUserType().equals("admin")) {
            ln_profile.setVisibility(View.GONE);
            ln_profile_view.setVisibility(View.GONE);
            ln_logout.setVisibility(View.VISIBLE);
            regdate.setText("This is " + userSession.getUserType() + " Login");
            reg_no.setVisibility(View.GONE);
        } else {
            ln_student.setVisibility(View.GONE);
            ln_logout.setVisibility(View.VISIBLE);
            ln_student_view.setVisibility(View.GONE);
            regdate.setText("Registered since " + userSession.getRegistrationDate() + "\nThis is " + userSession.getUserType() + " Login");
        }


        if (userSession.getUserType().equals("parent")) {
            ln_profile.setVisibility(View.GONE);
            ln_profile_view.setVisibility(View.GONE);
            txt_setting.setText("Switch Student");
        }

        ln_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                HomeActivity.IsProfileOpen = true;
                if (userSession.getRegisterUser()) {
                    replaceFragment(R.id.nav_host_fragment, new Profile(new Profile.OnItemClickListener() {
                        @Override
                        public void onItemClick(Bitmap item) {

                            Glide.with(HomeActivity.this).asBitmap().load(item).circleCrop().into(profile_image2);
                            Glide.with(HomeActivity.this).asBitmap().load(item).circleCrop().into(profile_image1);
                        }
                    }), "Fragment", null);
                } else {
                    TastyToast.makeText(getApplicationContext(), "Please Register with us to access this Feature", TastyToast.LENGTH_LONG, TastyToast.ERROR);
                }
                drawer.closeDrawer(Gravity.LEFT);
            }
        });
        ln_downloads.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                HomeActivity.IsProfileOpen = false;
                checkPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE, STORAGE_PERMISSION_CODE);

                if (userSession.getRegisterUser()) {
                    replaceFragment(R.id.nav_host_fragment, new DownloadImages(), "Fragment", null);
                } else {
                    TastyToast.makeText(getApplicationContext(), "Please Register with us to access this Feature", TastyToast.LENGTH_LONG, TastyToast.ERROR);
                }
                drawer.closeDrawer(Gravity.LEFT);
            }
        });

        ln_exam.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                HomeActivity.IsProfileOpen = false;
                if (userSession.getRegisterUser()) {
                    replaceFragment(R.id.nav_host_fragment, new Exam(), "Fragment", null);
                } else {
                    TastyToast.makeText(getApplicationContext(), "Please Register with us to access this Feature", TastyToast.LENGTH_LONG, TastyToast.ERROR);
                }
                drawer.closeDrawer(Gravity.LEFT);
            }
        });

        ln_attendance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                HomeActivity.IsProfileOpen = false;
                if (userSession.getRegisterUser()) {
                    if (userSession.getUserType().equals("admin")) {
                        replaceFragment(R.id.nav_host_fragment, new Attendance(), "Fragment", null);
                    } else {
                        replaceFragment(R.id.nav_host_fragment, new StudentAttendance(), "Fragment", null);
                    }
                } else {
                    TastyToast.makeText(getApplicationContext(), "Please Register with us to access this Feature", TastyToast.LENGTH_LONG, TastyToast.ERROR);
                }
                drawer.closeDrawer(Gravity.LEFT);

            }
        });

        ln_extra.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                HomeActivity.IsProfileOpen = false;
                replaceFragment(R.id.nav_host_fragment, new EXTRASCREEN(), "Fragment", null);
                drawer.closeDrawer(Gravity.LEFT);
            }
        });
        ln_schedule.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                HomeActivity.IsProfileOpen = false;
                replaceFragment(R.id.nav_host_fragment, new AddSchedule(), "Fragment", null);
                drawer.closeDrawer(Gravity.LEFT);
            }
        });

        ln_schedule_2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                HomeActivity.IsProfileOpen = false;
                if (userSession.getRegisterUser()) {
                    replaceFragment(R.id.nav_host_fragment, new Schedule(), "Fragment", null);
                } else {
                    TastyToast.makeText(getApplicationContext(), "Please Register with us to access this Feature", TastyToast.LENGTH_LONG, TastyToast.ERROR);
                }
                drawer.closeDrawer(Gravity.LEFT);
            }
        });

        ln_student.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                HomeActivity.IsProfileOpen = false;
                replaceFragment(R.id.nav_host_fragment, new StudentSearch(), "Fragment", null);
                drawer.closeDrawer(Gravity.LEFT);
            }
        });

        ln_announcements.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                HomeActivity.IsProfileOpen = false;
                if (userSession.getRegisterUser()) {
                    replaceFragment(R.id.nav_host_fragment, new Announcements(), "Fragment", null);
                } else {
                    TastyToast.makeText(getApplicationContext(), "Please Register with us to access this Feature", TastyToast.LENGTH_LONG, TastyToast.ERROR);
                }
                drawer.closeDrawer(Gravity.LEFT);
            }
        });
        ln_visa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                HomeActivity.IsProfileOpen = false;
                replaceFragment(R.id.nav_host_fragment, new Visa(), "Fragment", null);
                drawer.closeDrawer(Gravity.LEFT);
            }
        });


        NAME1.setText(userSession.getName() + " " + userSession.getLastName());
        NAME2.setText(userSession.getName() + " " + userSession.getLastName());
        NAME3.setText(userSession.getName() + " " + userSession.getLastName());
        NAME4.setText(userSession.getName() + " " + userSession.getLastName());
        if (userSession.getRegisterUser()) {
            reg_no.setText("Registration No : " + userSession.getRegistrationNumber());
        }else {
            reg_no.setVisibility(View.GONE);

        }
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.

        CircleImageView imageView = (CircleImageView) findViewById(R.id.profile_image1);
        CircleImageView imageView1 = (CircleImageView) findViewById(R.id.profile_image2);
        Glide.with(this).load(userSession.getProfile()).into(imageView1);
        Glide.with(this).load(userSession.getProfile()).into(imageView);
        // checkPermission();


        if (userSession.getRegisterUser()) {
            findViewById(R.id.btmLayout).setVisibility(View.GONE);
        } else {
            findViewById(R.id.btmLayout).setVisibility(View.VISIBLE);
        }


        findViewById(R.id.registerWithUs).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                HomeActivity.IsProfileOpen = false;
                GetRegisterWithUs();
            }
        });

        findViewById(R.id.bookADemoLecture).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                HomeActivity.IsProfileOpen = false;
                dialogForCategories = new Dialog(HomeActivity.this);
                dialogForCategories.setContentView(R.layout.custom_dialog_book_lecture);
                dialogForCategories.setCancelable(true);
                dialogForCategories.setCanceledOnTouchOutside(true);
                dialogForCategories.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                Window window = dialogForCategories.getWindow();
                window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);

                dateText = dialogForCategories.findViewById(R.id.dateEmail);
                standard = dialogForCategories.findViewById(R.id.standard);
                coaching_level = dialogForCategories.findViewById(R.id.coaching_level);
                batch = dialogForCategories.findViewById(R.id.batch);


                dialogForCategories.findViewById(R.id.dialogCategoryClose).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialogForCategories.dismiss();
                    }
                });

                dialogForCategories.findViewById(R.id.dateEmail).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        DialogFragment newFragment = new DateEmailPickerFragment();
                        newFragment.show(getSupportFragmentManager(), "dateEmailPicker");
                    }
                });


                GetStandard();
                GetBatch();
                standard.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                        standard_pos = i;
                        mDataset4.clear();
                        mDataset.clear();
// batch_layout.setVisibility(View.GONE);
                        GetCoachingLevel(mDataset2.get(standard_pos).getCoaching_id());
                        Log.e("standard_pos", "" + standard_pos + " " + mDataset2.size());
                        if (standard_pos != mDataset2.size() - 1) {
                            try {


                            } catch (Exception e) {
//GetStudnet("0","0");


                            }
                        }

                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> adapterView) {

                    }
                });

                coaching_level.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                        mDataset.clear();

                        Log.e("batch_pos", "" + coachin_pos + " " + mDataset.size());
                        if (coachin_pos != mDataset3.size() - 1) {
                            try {
                                coachin_pos = i;
// GetBatch(mDataset4.get(coachin_pos).getCoachinglevel_id());
                            } catch (Exception e) {
// GetStudnet("0","0");

                            }
                        }


                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> adapterView) {

                    }
                });

                batch.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {


                        batchId = mDataset.get(i).getBatch_id();

                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> adapterView) {

                    }
                });


                dialogForCategories.findViewById(R.id.btnBookLecture).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {


                        if (batchId.equals("")) {
                            Toast.makeText(dialogForCategories.getContext(), "Please fill require fields", Toast.LENGTH_LONG).show();
                        } else if (dateText.getText().toString().equals(" Select Demo Date:")) {
                            Toast.makeText(dialogForCategories.getContext(), "Please Select Date", Toast.LENGTH_LONG).show();
                        } else {

                            getBookDemoLecture(batchId, dateText.getText().toString());
                        }
                    }
                });


                dialogForCategories.show();

            }
        });

        findViewById(R.id.add_btn123).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                selectImage();
            }
        });
        findViewById(R.id.add_btn1234).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                selectImage();
            }
        });


    }


    private void GetBatch() {


        final KProgressHUD progressDialog = KProgressHUD.create(HomeActivity.this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("Please wait")
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();

        GetDemoBatchRequest getDemoBatchRequest = new GetDemoBatchRequest(new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("Response", response + " null");
                progressDialog.dismiss();
                mDataset.clear();
                JSONObject jsonObject = null;
                try {
                    jsonObject = new JSONObject(response);

                    JSONArray jsonArray = jsonObject.getJSONArray("data");

                    for (int i = 0; i < jsonArray.length(); i++) {


                        JSONObject object = jsonArray.getJSONObject(i);
                        BatchModel BatchModel = new BatchModel();
                        BatchModel.setBatch_id(object.getString("batch_id"));
                        BatchModel.setBatch_name(object.getString("batch_name"));
                        BatchModel.setBatch_time(object.getString("batch_time"));
                        BatchModel.setStatus(object.getString("status"));
                        BatchModel.setBranch_id(object.getString("branch_id"));
                        mDataset.add(BatchModel);


                    }
// batch_layout.setVisibility(View.VISIBLE);
                    BatchModel BatchModel = new BatchModel();
                    BatchModel.setBatch_id("");
                    BatchModel.setBatch_name("Please Select Batch");
                    BatchModel.setBatch_time("Please Select Batch");
                    BatchModel.setStatus("");
                    BatchModel.setBranch_id("");
                    mDataset.add(BatchModel);
                    SpinAdapter adapter = new SpinAdapter(HomeActivity.this,
                            android.R.layout.simple_spinner_item,
                            mDataset);


                    adapter.setDropDownViewResource(R.layout.spinner_item);
                    batch.setAdapter(adapter);
                    batch.setSelection(adapter.getCount());

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                progressDialog.dismiss();
                if (error instanceof ServerError)
                    Toast.makeText(HomeActivity.this, "Server Error", Toast.LENGTH_LONG).show();
                else if (error instanceof TimeoutError)
                    Toast.makeText(HomeActivity.this, "Connection Timed Out", Toast.LENGTH_LONG).show();
                else if (error instanceof NetworkError)
                    Toast.makeText(HomeActivity.this, "Bad Network Connection", Toast.LENGTH_LONG).show();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
// params.put("Accept", "application/json");
                params.put("Authorization", "Bearer " + userSession.getAPIToken());
                return params;
            }
        };
        getDemoBatchRequest.setTag("TAG");
        getDemoBatchRequest.setShouldCache(false);

        requestQueue.add(getDemoBatchRequest);


    }

    private void getBookDemoLecture(String batchId, String date) {

        final KProgressHUD progressDialog = KProgressHUD.create(HomeActivity.this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("Please wait")
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();

        GetDemoLecture getDemoLecture = new GetDemoLecture(batchId, date, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("Response", response + " null");
                progressDialog.dismiss();
                mDataset.clear();
                JSONObject jsonObject = null;
                try {
                    jsonObject = new JSONObject(response);

                    if (jsonObject.getString("ResponseCode").equals("200")) {
                        TastyToast.makeText(getApplicationContext(), jsonObject.getString("ResponseMsg"), TastyToast.LENGTH_LONG, TastyToast.SUCCESS);
                        dialogForCategories.cancel();
                    }


                } catch (JSONException e) {
                    dialogForCategories.cancel();
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                progressDialog.dismiss();
                if (error instanceof ServerError)
                    Toast.makeText(HomeActivity.this, "Server Error", Toast.LENGTH_LONG).show();
                else if (error instanceof TimeoutError)
                    Toast.makeText(HomeActivity.this, "Connection Timed Out", Toast.LENGTH_LONG).show();
                else if (error instanceof NetworkError)
                    Toast.makeText(HomeActivity.this, "Bad Network Connection", Toast.LENGTH_LONG).show();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                // params.put("Accept", "application/json");
                params.put("Authorization", "Bearer " + userSession.getAPIToken());
                return params;
            }
        };
        getDemoLecture.setTag("TAG");
        getDemoLecture.setShouldCache(false);
        getDemoLecture.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 60000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 60000;
            }

            @Override
            public void retry(VolleyError error) throws VolleyError {

            }
        });
        requestQueue.add(getDemoLecture);

    }

    private void GetStandard() {
        final KProgressHUD progressDialog = KProgressHUD.create(HomeActivity.this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("Please wait")
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();


        GetStandardRequest loginRequest1 = new GetStandardRequest(new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("Response", response + " null");
                progressDialog.dismiss();
                mDataset2.clear();
                JSONObject jsonObject = null;
                try {
                    jsonObject = new JSONObject(response);

                    JSONArray jsonArray = jsonObject.getJSONArray("data");

                    for (int i = 0; i < jsonArray.length(); i++) {

                        JSONObject object = jsonArray.getJSONObject(i);
                        StandardModel BatchModel = new StandardModel();
                        BatchModel.setCoaching_id(object.getString("coaching_id"));
                        BatchModel.setCoaching(object.getString("coaching"));
                        BatchModel.setStatus(object.getString("status"));
                        mDataset2.add(BatchModel);
                    }

                    StandardModel BatchModel = new StandardModel();
                    BatchModel.setCoaching_id("");
                    BatchModel.setCoaching("Please select standard");
                    BatchModel.setStatus("");
                    mDataset2.add(BatchModel);
                    SpinAdapter2 adapter = new SpinAdapter2(HomeActivity.this,
                            android.R.layout.simple_spinner_item,
                            mDataset2);
                    adapter.setDropDownViewResource(R.layout.spinner_item);
                    standard.setAdapter(adapter);
                    standard.setSelection(adapter.getCount());

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                progressDialog.dismiss();
                if (error instanceof ServerError)
                    Toast.makeText(HomeActivity.this, "Server Error", Toast.LENGTH_LONG).show();
                else if (error instanceof TimeoutError)
                    Toast.makeText(HomeActivity.this, "Connection Timed Out", Toast.LENGTH_LONG).show();
                else if (error instanceof NetworkError)
                    Toast.makeText(HomeActivity.this, "Bad Network Connection", Toast.LENGTH_LONG).show();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                // params.put("Accept", "application/json");
                params.put("Authorization", "Bearer " + userSession.getAPIToken());
                return params;
            }
        };
        loginRequest1.setTag("TAG");
        loginRequest1.setShouldCache(false);

        requestQueue.add(loginRequest1);
    }

    private void GetCoachingLevel(String Standard_id) {
        final KProgressHUD progressDialog = KProgressHUD.create(HomeActivity.this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("Please wait")
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();


        GetCoachingLevelRequest loginRequest1 = new GetCoachingLevelRequest(Standard_id, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("Response_coach", response + " null");

                mDataset4.clear();
                progressDialog.dismiss();
                JSONObject jsonObject = null;
                try {
                    jsonObject = new JSONObject(response);

                    JSONArray jsonArray = jsonObject.getJSONArray("data");

                    for (int i = 0; i < jsonArray.length(); i++) {

                        JSONObject object = jsonArray.getJSONObject(i);
                        CoachingLevelModel BatchModel = new CoachingLevelModel();
                        BatchModel.setCoachinglevel_id(object.getString("coachinglevel_id"));
                        BatchModel.setCoachinglevel(object.getString("coachinglevel"));
                        BatchModel.setCoaching_type_id(object.getString("coaching_type_id"));
                        BatchModel.setBranch_id(object.getString("branch_id"));
                        BatchModel.setStatus(object.getString("status"));
                        mDataset4.add(BatchModel);


                    }

                    CoachingLevelModel BatchModel = new CoachingLevelModel();
                    BatchModel.setCoachinglevel_id("");
                    BatchModel.setCoachinglevel("Please select level");
                    BatchModel.setCoaching_type_id("");
                    BatchModel.setBranch_id("");
                    BatchModel.setStatus("");
                    mDataset4.add(BatchModel);
                    CoatchingLevelAdapter adapter = new CoatchingLevelAdapter(HomeActivity.this,
                            android.R.layout.simple_spinner_item,
                            mDataset4);
                    adapter.setDropDownViewResource(R.layout.spinner_item);
                    coaching_level.setAdapter(adapter);
                    coaching_level.setSelection(adapter.getCount());

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                progressDialog.dismiss();
                if (error instanceof ServerError)
                    Toast.makeText(HomeActivity.this, "Server Error", Toast.LENGTH_LONG).show();
                else if (error instanceof TimeoutError)
                    Toast.makeText(HomeActivity.this, "Connection Timed Out", Toast.LENGTH_LONG).show();
                else if (error instanceof NetworkError)
                    Toast.makeText(HomeActivity.this, "Bad Network Connection", Toast.LENGTH_LONG).show();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                // params.put("Accept", "application/json");
                params.put("Authorization", "Bearer " + userSession.getAPIToken());
                return params;
            }
        };
        loginRequest1.setTag("TAG");
        loginRequest1.setShouldCache(false);

        requestQueue.add(loginRequest1);
    }

    private void GetRegisterWithUs() {


        final KProgressHUD progressDialog = KProgressHUD.create(HomeActivity.this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("Please wait")
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();


        GetRegisterWithUs getRegisterWithUs = new GetRegisterWithUs(new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("Response1", response + " null");
                progressDialog.dismiss();

                JSONObject jsonObject = null;
                try {
                    jsonObject = new JSONObject(response);

                    //   JSONObject jsonObject1 = jsonObject.getJSONObject("ResponseMsg");

                    if (jsonObject.getString("ResponseCode").equals("200")) {
                        TastyToast.makeText(getApplicationContext(), jsonObject.getString("ResponseMsg"), TastyToast.LENGTH_LONG, TastyToast.SUCCESS);

                    }


                } catch (JSONException e) {

                    Toast.makeText(HomeActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                progressDialog.dismiss();
                userSession.logout();
                Intent intent = new Intent(HomeActivity.this, Login_Activity.class);
                startActivity(intent);
                finish();
                if (error instanceof ServerError)
                    Toast.makeText(HomeActivity.this, "Server Error", Toast.LENGTH_LONG).show();
                else if (error instanceof TimeoutError)
                    Toast.makeText(HomeActivity.this, "Connection Timed Out", Toast.LENGTH_LONG).show();
                else if (error instanceof NetworkError)
                    Toast.makeText(HomeActivity.this, "Bad Network Connection", Toast.LENGTH_LONG).show();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Accept", "application/json");
                params.put("Authorization", "Bearer " + userSession.getAPIToken());
                return params;
            }
        };
        getRegisterWithUs.setTag("TAG");
        getRegisterWithUs.setShouldCache(false);
        getRegisterWithUs.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 60000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 60000;
            }

            @Override
            public void retry(VolleyError error) throws VolleyError {

            }
        });
        requestQueue.add(getRegisterWithUs);

    }


    private void GetBatch(String level) {


        final KProgressHUD progressDialog = KProgressHUD.create(HomeActivity.this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("Please wait")
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();

        GetBatchRequest loginRequest = new GetBatchRequest(level, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("Response", response + " null");
                progressDialog.dismiss();
                mDataset.clear();
                JSONObject jsonObject = null;
                try {
                    jsonObject = new JSONObject(response);

                    JSONArray jsonArray = jsonObject.getJSONArray("data");

                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject object = jsonArray.getJSONObject(i);
                        BatchModel BatchModel = new BatchModel();
                        BatchModel.setBatch_id(object.getString("batch_id"));
                        BatchModel.setBatch_name(object.getString("batch_name"));
                        BatchModel.setBatch_time(object.getString("batch_time"));
                        BatchModel.setStatus(object.getString("status"));
                        BatchModel.setBranch_id(object.getString("branch_id"));
                        mDataset.add(BatchModel);
                    }
                    // batch_layout.setVisibility(View.VISIBLE);
                    BatchModel BatchModel = new BatchModel();
                    BatchModel.setBatch_id("");
                    BatchModel.setBatch_name("Please Select Batch");
                    BatchModel.setBatch_time("Please Select Batch");
                    BatchModel.setStatus("");
                    BatchModel.setBranch_id("");
                    mDataset.add(BatchModel);
                    SpinAdapter adapter = new SpinAdapter(HomeActivity.this,
                            android.R.layout.simple_spinner_item,
                            mDataset);


                    adapter.setDropDownViewResource(R.layout.spinner_item);
                    batch.setAdapter(adapter);
                    batch.setSelection(adapter.getCount());

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                progressDialog.dismiss();
                if (error instanceof ServerError)
                    Toast.makeText(HomeActivity.this, "Server Error", Toast.LENGTH_LONG).show();
                else if (error instanceof TimeoutError)
                    Toast.makeText(HomeActivity.this, "Connection Timed Out", Toast.LENGTH_LONG).show();
                else if (error instanceof NetworkError)
                    Toast.makeText(HomeActivity.this, "Bad Network Connection", Toast.LENGTH_LONG).show();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Accept", "application/json");
                params.put("Authorization", "Bearer " + userSession.getAPIToken());
                return params;
            }
        };
        loginRequest.setTag("TAG");
        loginRequest.setShouldCache(false);

        requestQueue.add(loginRequest);


    }

    @Override
    public void onItemClick(Bitmap item) {
        Glide.with(HomeActivity.this).asBitmap().load(item).circleCrop().into(profile_image2);
        Glide.with(HomeActivity.this).asBitmap().load(item).circleCrop().into(profile_image1);


    }


    public static class DateEmailPickerFragment extends DialogFragment
            implements DatePickerDialog.OnDateSetListener {


        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            final Calendar c = Calendar.getInstance();
            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH);
            int day = c.get(Calendar.DAY_OF_MONTH);
            DatePickerDialog dialog = new DatePickerDialog(getActivity(), this, year, month, day);
            dialog.getDatePicker().setMinDate(c.getTimeInMillis());
           /* c.add(Calendar.DAY_OF_MONTH,2);
            dialog.getDatePicker().setMaxDate(c.getTimeInMillis());*/
            return dialog;
        }

        public void onDateSet(DatePicker view, int year, int month, int day) {
            Calendar calendar = Calendar.getInstance();
            calendar.set(year, month, day);
            SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
            String dateString = dateFormat.format(calendar.getTime());
            /*	try {
             *//*if (dateFormat.parse(formattedDate_abc).after(dateFormat.parse(dateString))) {
						Toast.makeText(getActivity(), "Please select correct date", Toast.LENGTH_LONG).show();
						return;
					}*//*

				    Date oldDate = null;
					oldDate = dateFormat.parse(dateString);
					Date currentDate = new Date();
					String newDate1 = dateFormat.format(currentDate);
					Date newDate = dateFormat.parse(newDate1);

					long diff =  oldDate.getTime() - newDate.getTime();
					long diffInHours = TimeUnit.MILLISECONDS.toDays(diff);

					if(diffInHours>2){
						Toast.makeText(getActivity(), "Please select correct date", Toast.LENGTH_LONG).show();
						return;
					}


			} catch (ParseException e) {
				e.printStackTrace();
			}*/
            dateText.setText(dateString);


        }
    }


    protected void addFragment(@IdRes int containerViewId,
                               @NonNull Fragment fragment,
                               @NonNull String fragmentTag) {
        getSupportFragmentManager()
                .beginTransaction()
                .add(containerViewId, fragment, fragmentTag)
                .disallowAddToBackStack()
                .commit();
    }

    protected void replaceFragment(@IdRes int containerViewId,
                                   @NonNull Fragment fragment,
                                   @NonNull String fragmentTag,
                                   @Nullable String backStackStateName) {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(containerViewId, fragment, fragmentTag)
                .disallowAddToBackStack()
                .commit();

    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    public boolean checkPermission() {
        int currentAPIVersion = Build.VERSION.SDK_INT;
        if (currentAPIVersion >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(HomeActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                if (ActivityCompat.shouldShowRequestPermissionRationale(HomeActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                    AlertDialog.Builder alertBuilder = new AlertDialog.Builder(HomeActivity.this);
                    alertBuilder.setCancelable(true);
                    alertBuilder.setTitle("Permission necessary");
                    alertBuilder.setMessage("Write Storage permission is necessary to Download Images and Videos!!!");
                    alertBuilder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                        @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
                        public void onClick(DialogInterface dialog, int which) {
                            ActivityCompat.requestPermissions(HomeActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_WRITE_STORAGE);
                        }
                    });
                    AlertDialog alert = alertBuilder.create();
                    alert.show();
                } else {
                    ActivityCompat.requestPermissions(HomeActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_WRITE_STORAGE);
                }
                return false;
            } else {
                return true;
            }
        } else {
            return true;
        }
    }

    public static final int MY_PERMISSIONS_REQUEST_WRITE_STORAGE = 123;
    boolean result;
    //Here you can check App Permission

    public void checkAgain() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(HomeActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            AlertDialog.Builder alertBuilder = new AlertDialog.Builder(HomeActivity.this);
            alertBuilder.setCancelable(true);
            alertBuilder.setTitle("Permission necessary");
            alertBuilder.setMessage("Write Storage permission is necessary to Download Images and Videos!!!");
            alertBuilder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
                public void onClick(DialogInterface dialog, int which) {
                    ActivityCompat.requestPermissions(HomeActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_WRITE_STORAGE);
                }
            });
            AlertDialog alert = alertBuilder.create();
            alert.show();
        } else {
            ActivityCompat.requestPermissions(HomeActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_WRITE_STORAGE);
        }
    }


   /* @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        for (Fragment fragment : getSupportFragmentManager().getFragments()) {
            fragment.onActivityResult(requestCode, resultCode, data);
        }
    }*/

    private void clearAppData() {
        try {
            // clearing app data
            if (Build.VERSION_CODES.KITKAT <= Build.VERSION.SDK_INT) {
                ((ActivityManager) getSystemService(ACTIVITY_SERVICE)).clearApplicationUserData(); // note: it has a return value!
            } else {
                String packageName = getApplicationContext().getPackageName();
                Runtime runtime = Runtime.getRuntime();
                runtime.exec("pm clear " + packageName);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void restartApp() {
        Intent intent = new Intent(getApplicationContext(), Login_Activity.class);
        int mPendingIntentId = 2;
        PendingIntent mPendingIntent = PendingIntent.getActivity(getApplicationContext(), mPendingIntentId, intent, PendingIntent.FLAG_CANCEL_CURRENT);
        AlarmManager mgr = (AlarmManager) getApplicationContext().getSystemService(Context.ALARM_SERVICE);
        mgr.set(AlarmManager.RTC, System.currentTimeMillis() + 100, mPendingIntent);
        System.exit(0);
    }

    public void checkPermission(String permission, int requestCode) {

        // Checking if permission is not granted
        if (ContextCompat.checkSelfPermission(
                HomeActivity.this,
                permission)
                == PackageManager.PERMISSION_DENIED) {
            ActivityCompat
                    .requestPermissions(
                            HomeActivity.this,
                            new String[]{permission},
                            requestCode);
        } else {
           /* Toast
                    .makeText(HomeActivity.this,
                            "Permission already granted",
                            Toast.LENGTH_LONG)
                    .show();*/
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        super
                .onRequestPermissionsResult(requestCode,
                        permissions,
                        grantResults);

        if (requestCode == CAMERA_PERMISSION_CODE) {

            // Checking whether user granted the permission or not.
            if (grantResults.length > 0
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                // Showing the toast message
                Toast.makeText(HomeActivity.this,
                        "Camera Permission Granted",
                        Toast.LENGTH_LONG)
                        .show();


            } else {
                Toast.makeText(HomeActivity.this,
                        "Camera Permission Denied",
                        Toast.LENGTH_LONG)
                        .show();
            }
        } else if (requestCode == STORAGE_PERMISSION_CODE) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(HomeActivity.this,
                        "Storage Permission Granted",
                        Toast.LENGTH_LONG)
                        .show();

                File myDirectory = new File(Environment.getRootDirectory(), "ShreeHari");

                if (!myDirectory.exists()) {
                    myDirectory.mkdirs();
                }
            } else {
                Toast.makeText(HomeActivity.this,
                        "Storage Permission Denied",
                        Toast.LENGTH_LONG)
                        .show();
            }
        }
    }


    private void status(String status) {
        HashMap<String, Object> hashMap = new HashMap<>();
        hashMap.put("status", status);
        hashMap.put("type", userSession.getUserType());
        hashMap.put("imageURL", userSession.getProfile());

        reference.updateChildren(hashMap);
    }


    @Override
    protected void onResume() {
        super.onResume();
        status("online");
    }

    @Override
    protected void onPause() {
        super.onPause();
        status("offline");
    }


    private ImageView profile_image2;
    private InputStream inputStreamImg;
    public static final int REQUEST_ID_MULTIPLE_PERMISSIONS2 = 7;
    private File destination = null;
    private String imgPath = null;

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        inputStreamImg = null;
        if (requestCode == PICK_IMAGE_CAMERA) {
            try {
                Uri selectedImage = data.getData();
                Bitmap bitmap = (Bitmap) data.getExtras().get("data");
                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 50, bytes);

                Log.e("Activity", "Pick from Camera::>>> ");

                File folder = new File(Environment.getExternalStorageDirectory() +
                        File.separator + "ShriHari");
                if (!folder.exists()) {
                    folder.mkdirs();
                }
                String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(new Date());
                destination = new File(Environment.getExternalStorageDirectory() + "/" +
                        "ShriHari", "IMG_" + timeStamp + ".jpg");

                Log.e("destinationFile", destination + "--");

                FileOutputStream fo;
                try {
                    destination.createNewFile();
                    fo = new FileOutputStream(destination);
                    fo.write(bytes.toByteArray());
                    fo.close();
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                imgPath = destination.getAbsolutePath();
                Glide.with(HomeActivity.this).asBitmap().load(bitmap).circleCrop().into(profile_image1);
                Glide.with(HomeActivity.this).asBitmap().load(bitmap).circleCrop().into(profile_image2);
                UploadDatatoServer(bitmap);
                //txt_injury.setText(imgPath);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (requestCode == PICK_IMAGE_GALLERY) {
            Uri selectedImage = data.getData();
            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(HomeActivity.this.getContentResolver(), selectedImage);
                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 50, bytes);
                Log.e("Activity", "Pick from Gallery::>>> ");

                imgPath = getRealPathFromURI(selectedImage);
                destination = new File(imgPath.toString());

                Log.e("destinationFile", destination + "--");

                //  txt_injury.setText(imgPath);

                Glide.with(HomeActivity.this).asBitmap().load(bitmap).circleCrop().into(profile_image1);
                Glide.with(HomeActivity.this).asBitmap().load(bitmap).circleCrop().into(profile_image2);
                UploadDatatoServer(bitmap);


            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public String getRealPathFromURI(Uri contentUri) {
        String[] proj = {MediaStore.Audio.Media.DATA};
        Cursor cursor = managedQuery(contentUri, proj, null, null, null);
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Audio.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }

    private final int PICK_IMAGE_CAMERA = 1, PICK_IMAGE_GALLERY = 2;

    private void selectImage() {
        try {
            PackageManager pm = HomeActivity.this.getPackageManager();
            int hasPerm = pm.checkPermission(Manifest.permission.CAMERA, HomeActivity.this.getPackageName());
            if (hasPerm == PackageManager.PERMISSION_GRANTED) {
                final CharSequence[] options = {getString(R.string.take_photo), getString(R.string.gallary), getString(R.string.cancel)};
                AlertDialog.Builder builder = new AlertDialog.Builder(HomeActivity.this);
                builder.setTitle(R.string.select_option);
                builder.setItems(options, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int item) {
                        if (options[item].equals(getResources().getString(R.string.take_photo))) {
                            dialog.dismiss();
                            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                            startActivityForResult(intent, PICK_IMAGE_CAMERA);
                        } else if (options[item].equals(getResources().getString(R.string.gallary))) {
                            dialog.dismiss();
                            Intent pickPhoto = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                            startActivityForResult(pickPhoto, PICK_IMAGE_GALLERY);
                        } else if (options[item].equals(getResources().getString(R.string.cancel))) {
                            dialog.dismiss();
                        }
                    }
                });
                builder.show();
            } else
                checkAndroidVersion();
            //Toast.makeText(IELTSExamBook.this, "Camera Permission error", Toast.LENGTH_LONG).show();
        } catch (Exception e) {
            checkAndroidVersion();
            //Toast.makeText(IELTSExamBook.this, "Camera Permission error", Toast.LENGTH_LONG).show();
            e.printStackTrace();
        }
    }

    private void checkAndroidVersion() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            checkAndRequestPermissions();

        } else {
            // code for lollipop and pre-lollipop devices
        }
    }

    private boolean checkAndRequestPermissions() {
        int camera = ContextCompat.checkSelfPermission(HomeActivity.this,
                Manifest.permission.CAMERA);
        int wtite = ContextCompat.checkSelfPermission(HomeActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        int read = ContextCompat.checkSelfPermission(HomeActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE);
        List<String> listPermissionsNeeded = new ArrayList<>();
        if (wtite != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }
        if (camera != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.CAMERA);
        }
        if (read != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.READ_EXTERNAL_STORAGE);
        }
        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(HomeActivity.this, listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), REQUEST_ID_MULTIPLE_PERMISSIONS2);
            return false;
        }
        return true;
    }

    private void UploadDatatoServer(Bitmap bitmap) {
        final KProgressHUD progressDialog = KProgressHUD.create(HomeActivity.this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("Please wait")
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();

        VolleyMultipartRequest volleyMultipartRequest = new VolleyMultipartRequest(Request.Method.POST, ServerUtils.BASE_URL + "update-profile-pic",
                new Response.Listener<NetworkResponse>() {
                    @Override
                    public void onResponse(NetworkResponse response) {

                        progressDialog.dismiss();

                        try {


                            JSONObject jsonObject = new JSONObject(new String(response.data));
                            Log.e("Response", jsonObject.toString() + " --");
                            if (jsonObject.getString("ResponseCode").equals("200")) {

                                userSession.setProfile(jsonObject.getJSONObject("data").getString("profile_pic"));

                                if(IsProfileOpen){
                                    replaceFragment(R.id.nav_host_fragment, new Profile(new Profile.OnItemClickListener() {
                                        @Override
                                        public void onItemClick(Bitmap item) {

                                        }
                                    }), "Fragment", null);
                                }
                                try {


                                    Toast.makeText(HomeActivity.this, jsonObject.getString("ResponseMsg"), Toast.LENGTH_LONG).show();
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                            }

                        } catch (Exception e) {
                            Toast.makeText(HomeActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();

                           /* session.logout();
                            Intent intent = new Intent(Activity_ManageMyWishList.this, Activity_SelectCity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                            finish();*/
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        try {
                            String responseBody = new String(error.networkResponse.data, "utf-8");
                            JSONObject data = new JSONObject(responseBody);
                            // JSONArray errors = data.getJSONArray("errors");
                            // JSONObject jsonMessage = data.getJSONObject(0);

                            Log.e("ErrorResponse", data.toString() + "");
                            String message = data.getString("message");
                            Toast.makeText(HomeActivity.this, message, Toast.LENGTH_LONG).show();
                        } catch (JSONException e) {
                        } catch (UnsupportedEncodingException errorr) {
                        }
                    }
                }) {


            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Accept", "application/json");
                params.put("Authorization", "Bearer " + userSession.getAPIToken());
                return params;
            }


            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();
                long imagename = System.currentTimeMillis();
                params.put("profile_pic", new DataPart(imagename + ".png", getFileDataFromDrawable(bitmap)));


                return params;
            }
        };
        //adding the request to volley
        volleyMultipartRequest.setShouldCache(false);
        requestQueue.add(volleyMultipartRequest);
    }

    public byte[] getFileDataFromDrawable(Bitmap bitmap) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 80, byteArrayOutputStream);
        return byteArrayOutputStream.toByteArray();
    }


}