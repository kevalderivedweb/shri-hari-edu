package com.mystudycanada.shreehari.ui;

import com.android.volley.AuthFailureError;
import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;
import com.mystudycanada.shreehari.API.ServerUtils;

import java.util.HashMap;
import java.util.Map;

public class GetDemoBatchRequest extends StringRequest {

    private Map<String, String> parameters;

    public GetDemoBatchRequest(Response.Listener<String> listener, Response.ErrorListener errorListener) {
        super(Method.GET, ServerUtils.BASE_URL+"get-demo-batch", listener, errorListener);
        parameters = new HashMap<>();

    }


    @Override
    protected Map<String, String> getParams() throws AuthFailureError {
        return parameters;
    }

}
