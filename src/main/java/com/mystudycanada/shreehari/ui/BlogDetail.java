package com.mystudycanada.shreehari.ui;

import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.mystudycanada.shreehari.API.GetBlogDetail;
import com.mystudycanada.shreehari.R;
import com.mystudycanada.shreehari.UserSession.UserSession;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class BlogDetail extends Fragment {

    private RequestQueue requestQueue;
    private UserSession session;

    private String blogDetailId;

    private TextView blogDetailTitle, textBlogDetail;
    private ImageView blogImage;

    public BlogDetail(String blogDetailId) {
        this.blogDetailId = blogDetailId;
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_blog_detail, container, false);

        requestQueue = Volley.newRequestQueue(getActivity());//Creating the RequestQueue
        session = new UserSession(getActivity());

        blogDetailTitle = view.findViewById(R.id.blogDetailTitle);
        textBlogDetail = view.findViewById(R.id.textBlogDetail);
        blogImage = view.findViewById(R.id.blogImage);

        GetBlogDetail(blogDetailId);

        return view;

    }


    private void GetBlogDetail(String blogDetailId) {

        final KProgressHUD progressDialog = KProgressHUD.create(getActivity())
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("Please wait")
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();

        GetBlogDetail getBlogDetail = new GetBlogDetail(blogDetailId, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("Response", response + " null");
                progressDialog.dismiss();

                JSONObject jsonObject = null;
                try {
                    jsonObject = new JSONObject(response);

                    if (jsonObject.getString("ResponseCode").equals("200")){

                        JSONObject object = jsonObject.getJSONObject("data");

                        Glide.with(getContext()).load(object.getString("document")).into(blogImage);

                        blogDetailTitle.setText(object.getString("title"));

                        textBlogDetail.setText(Html.fromHtml(object.getString("description")));


                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                progressDialog.dismiss();
                if (error instanceof ServerError)
                    Toast.makeText(getActivity(), "Server Error", Toast.LENGTH_LONG).show();
                else if (error instanceof TimeoutError)
                    Toast.makeText(getActivity(), "Connection Timed Out", Toast.LENGTH_LONG).show();
                else if (error instanceof NetworkError)
                    Toast.makeText(getActivity(), "Bad Network Connection", Toast.LENGTH_LONG).show();
            }
        }){@Override
        public Map<String, String> getHeaders() throws AuthFailureError {
            Map<String, String> params = new HashMap<String, String>();
// params.put("Accept", "application/json");
            params.put("Authorization","Bearer "+ session.getAPIToken());
            return params;
        }};
        getBlogDetail.setTag("TAG");
        getBlogDetail.setShouldCache(false);
        getBlogDetail.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 60000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 60000;
            }

            @Override
            public void retry(VolleyError error) throws VolleyError {

            }
        });
        requestQueue.add(getBlogDetail);



    }


}
