package com.mystudycanada.shreehari.ui;

import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import com.mystudycanada.shreehari.R;

public class HowHardIelts extends Fragment {

    private TextView hardIeltsText;

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_how_hard_ielts, container, false);

        hardIeltsText = view.findViewById(R.id.hardIeltsText);

        view.findViewById(R.id.back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //   getFragmentManager().popBackStack();

                FragmentManager fm = getFragmentManager();
                if (fm.getBackStackEntryCount() > 0) {
                    Log.i("MainActivity", "popping backstack");
                    fm.popBackStack();
                } else {
                    Log.i("MainActivity", "nothing on backstack, calling super");
                }

            }
        });

        hardIeltsText.setText(Html.fromHtml("<div class=\"elementor-element elementor-element-7c49f51 elementor-widget elementor-widget-text-editor\" data-id=\"7c49f51\" data-element_type=\"widget\" data-widget_type=\"text-editor.default\">\n" +
                "<div class=\"elementor-widget-container\">\n" +
                "<div class=\"elementor-text-editor elementor-clearfix\">\n" +
                "<div class=\"elementor-element elementor-element-7c49f51 elementor-widget elementor-widget-text-editor\" data-id=\"7c49f51\" data-element_type=\"widget\" data-widget_type=\"text-editor.default\">\n" +
                "<div class=\"elementor-widget-container\">\n" +
                "<div class=\"elementor-text-editor elementor-clearfix\">\n" +
                "<p>IELTS is not that difficult as people perceive it to be. It&rsquo;s like any other. The questions are straight-forward and are meant to assess how well you can use your English. None of the questions is meant for tricking or testing a candidate&rsquo;s opinions. Recall also that IELTS does not have a pass or fail. The findings are recorded on a scale of 9 bands (1 being the lowest, 9 the highest).</p>\n" +
                "<p>This scale has remained consistent for over 20 years. The score you need is determined by your visa criteria, or the institution / organization you apply to. Please remember to review the appropriate score before taking the exam.</p>\n" +
                "</div>\n" +
                "</div>\n" +
                "</div>\n" +
                "<div class=\"elementor-element elementor-element-e51fdd8 elementor-widget elementor-widget-text-editor\" data-id=\"e51fdd8\" data-element_type=\"widget\" data-widget_type=\"text-editor.default\">\n" +
                "<div class=\"elementor-widget-container\">\n" +
                "<div class=\"elementor-text-editor elementor-clearfix\">\n" +
                "<p><em><strong>IELTS test assesses the following abilities of a candidate:</strong></em></p>\n" +
                "</div>\n" +
                "</div>\n" +
                "</div>\n" +
                "<div class=\"elementor-element elementor-element-1496727 elementor-icon-list--layout-traditional elementor-widget elementor-widget-icon-list\" data-id=\"1496727\" data-element_type=\"widget\" data-widget_type=\"icon-list.default\">\n" +
                "<div class=\"elementor-widget-container\">\n" +
                "<ul class=\"elementor-icon-list-items\">\n" +
                "> Candidate's ability to understand a speaker's attitude and opinion.</span></li>\n" +
                "</ul>\n" +
                "</div>\n" +
                "</div>\n" +
                "<div class=\"elementor-element elementor-element-d060836 elementor-icon-list--layout-traditional elementor-widget elementor-widget-icon-list\" data-id=\"d060836\" data-element_type=\"widget\" data-widget_type=\"icon-list.default\">\n" +
                "<div class=\"elementor-widget-container\">\n" +
                "<ul class=\"elementor-icon-list-items\">\n" +
                "> Candidate's ability to understand specific points or an overall understanding of the key points of the text.</span></li>\n" +
                "</ul>\n" +
                "</div>\n" +
                "</div>\n" +
                "<div class=\"elementor-element elementor-element-902e77f elementor-icon-list--layout-traditional elementor-widget elementor-widget-icon-list\" data-id=\"902e77f\" data-element_type=\"widget\" data-widget_type=\"icon-list.default\">\n" +
                "<div class=\"elementor-widget-container\">\n" +
                "<ul class=\"elementor-icon-list-items\">\n" +
                "> Candidate's ability to communicate in English using complex and abstract ideas.</span></li>\n" +
                "</ul>\n" +
                "</div>\n" +
                "</div>\n" +
                "<div class=\"elementor-element elementor-element-30e3938 elementor-widget elementor-widget-text-editor\" data-id=\"30e3938\" data-element_type=\"widget\" data-widget_type=\"text-editor.default\">\n" +
                "<div class=\"elementor-widget-container\">\n" +
                "<div class=\"elementor-text-editor elementor-clearfix\">\n" +
                "<p>IELTS test is not tough if a candidate is well-prepared. Candidate should be familiar with the topics that could be asked or expected in the test. Unfamiliarity with the format of the IELTS makes it difficult.</p>\n" +
                "</div>\n" +
                "</div>\n" +
                "</div>\n" +
                "</div>\n" +
                "</div>\n" +
                "</div>"));

        return view;
    }

}