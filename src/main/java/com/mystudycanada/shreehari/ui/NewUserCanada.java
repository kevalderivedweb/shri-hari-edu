package com.mystudycanada.shreehari.ui;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;

import androidx.fragment.app.Fragment;

import com.mystudycanada.shreehari.Adapter.ExpListAdapterFrequentAsked;
import com.mystudycanada.shreehari.R;

import java.util.ArrayList;

public class NewUserCanada extends Fragment {

    private ArrayList<String> questionsArrayList = new ArrayList<>();
    private ArrayList<String> answersArrayList = new ArrayList<>();
    private ExpandableListView expandableListView;
    private ExpListAdapterFrequentAsked expListAdapterFrequentAsked;

    private String country;

    private int lastExpandedPosition = -1;

    public NewUserCanada(String country){
        this.country = country;
    }


    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_new_user_canada, container, false);

        expandableListView = view.findViewById(R.id.lvExpQuestions);
        expandableListView.setChildDivider(getResources().getDrawable(R.color.white));



        expListAdapterFrequentAsked = new ExpListAdapterFrequentAsked(getActivity(), questionsArrayList, answersArrayList);
        // setting list adapter
        expandableListView.setAdapter(expListAdapterFrequentAsked);
        // Listview Group click listener
        expandableListView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {

            @Override
            public boolean onGroupClick(ExpandableListView parent, View v,
                                        int groupPosition, long id) {
                // Toast.makeText(getApplicationContext(),
                // "Group Clicked " + listDataHeader.get(groupPosition),
                // Toast.LENGTH_LONG).show();



                return false;
            }
        });

        canadaArrayData();

        expListAdapterFrequentAsked.notifyDataSetChanged();

        // Listview on child click listener
        expandableListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {

            @Override
            public boolean onChildClick(ExpandableListView parent, View v,
                                        int groupPosition, int childPosition, long id) {
                // TODO Auto-generated method stub

                return false;
            }
        });

        return view;
    }

    private void canadaArrayData() {

        if (country.equals("canada")) {
            questionsArrayList.add("| ABOUT CANADA AND ITS GEOGRAPHY:");
            questionsArrayList.add("| CANADIAN POLITICAL SYSTEM:");
            questionsArrayList.add("| CANADA’s ECONOMY:");
            questionsArrayList.add("| WHY CANADA?");
            questionsArrayList.add("| EDUCATION SYSTEM - TYPES OF COURSES IN CANADA:");

            answersArrayList.add("<p>With a total space of 9.9 million square kilometer, CANADA is the 2<sup>nd</sup> largest country after RUSSIA. It&rsquo;s situated within the prime 1/2 of NORTH-AMERICA, and it borders the country by 3 oceans: Pacific, Atlantic, and The Arctic. In fact, outline of CANADA is the longest as compared to alternative countries. They share virtually 9000 km. of Land border with UNITED STATES (U.S.). Amazingly, it&rsquo;s the longest border within the world. Most of Canadians reside inside several hundred km of the U.S. border.</p>\n" +
                    "<p>Home to forty- six national parks and many provincial parks, the immense land in Northern NORTH AMERICA is affluent with natural resources. CANADA could be a natural playground for flora &amp; fauna.</p>\n" +
                    "<p>Encircling urban with an abundance of nature and life, CANADA is a land of made diversity. It&rsquo;s exciting however safe, incautious however stable. It promotes trailblazing technology, exalting cultural icons, and a vivacious society that&rsquo;s receptive to everyone. Canadians tend to be hospitable, broad-minded, and trendy.</p>\n" +
                    "<p>CANADA is graded as one of the most effective countries to live in the world by UNITED NATIONS. Globally CANADA is acknowledged for its stability, progressive political setting, wonderful quality of life, and one among the healthiest economies within the world. Technologically and Economically development of the state is parallel with The U.S., it&rsquo;s neighbor to the South across an associate unfortified border.</p>\n" +
                    "<p><strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </strong></p>\n" +
                    "<p><strong><em>Language: </em></strong></p>\n" +
                    "<p>FRENCH &amp; ENGLISH are the two official languages of CANADA. Bilingual services are offered by all federal establishments and plenty of businesses. Followed by Punjabi, Spanish, Arabic, and Tagalog. Chinese dialects are the third common linguistic communication in CANADA. The foremost common endemic languages are Cree, Inuktitut, and Innu.</p>\n" +
                    "<p>The name &ldquo;CANADA&rdquo; comes from the Huron-Iroquois word &ldquo;KANATA&rdquo;, which means &lsquo;Village&rsquo; or &lsquo;Settlement&rsquo;. In 1791 the primary use of CANADA an official name came into existence. It is said that a French Explorer JACQUES CARTIER used the word &ldquo;CANADA&rdquo; because of the shortage of another name.</p>\n" +
                    "<p><strong>&nbsp;</strong></p>\n" +
                    "<p><strong><em>Population of Canada:</em></strong></p>\n" +
                    "<p>CANADA ranks Thirty- Eighth by population that is around 36.7 million. Around 80% of individuals residein cities and towns. CANADA has most house space that its population density ratio is one amongst the bottom within the world. That interprets to less than four persons per square kilometer.</p>\n" +
                    "<p>&nbsp;Largest cities of CANADA&rsquo;S include:</p>\n" +
                    "<ul>\n" +
                    "<li>TORONTO: 6.3 Million</li>\n" +
                    "<li>MONTREAL: 4.1 Million</li>\n" +
                    "<li>VANCOUVER: 2.6 Million</li>\n" +
                    "<li>CALGARY: 1.5 Million</li>\n" +
                    "<li>EDMONTON: 1.4 Million</li>\n" +
                    "<li>OTTAWA-GATINEAU: 1.4 Million</li>\n" +
                    "</ul>\n" +
                    "<p>CANADA is a fusion of assorted cultures with ethnic diversity. Canadians represent 250 ethnic origins or ancestries. There are Twenty Million Canadians of European Ancestry, Two Million with Indigenous ancestry, and enormous numbers of Canadians who report being of Chinese, East Indians, Geographic region, and alternative ancestries. CANADA&rsquo;S Indigenous population consists of initial Nations, Inuit and Metis people</p>\n" +
                    "<p>As mentioned earlier CANADA is a fusion of various cultures, many various religions are practiced here, whereas, some claim no spiritual affiliation. Canadian population includes folk that clutches to catholic, protestant, Christian, Hindu, Sikh, and alternative faiths.</p>\n" +
                    "<p>&nbsp;</p>\n" +
                    "<p><strong><em>Climate:</em></strong></p>\n" +
                    "<p>CANADA is usually connected with snow and cold atmospheric condition, however, in point of fact, its climate is as diverse as its landscapes. Canadians usually fancy Four different seasons, notably within a lot of inhabited regions on the U.S. border.</p>\n" +
                    "<p>There is heat spring hot summer between June to August and pleasant refreshing autumns in September. Winter usually runs from November to March;however, the suitable dates and lowest temperatures can vary across the country.</p>\n" +
                    "<p>Canadians embrace CANADA&rsquo;S seasonal temperature by numerous leisure activities to settle on, from swimming outdoors within the Summer to sport within the Winter. The buildings within which foreign students can study are well heated in winters. Several faculty campuses and universities have underground tunnels or lined bridges through that students can move from building to assembly within the winters to urge shielded from cold. Overall, winters in CANADA are endearing, and it&rsquo;s very simple to remain heated just by dressing properly once going outdoors. Students who are studying in CANADA should be suggested by agents to take a position in winter wear during the wintertime, including a warm coat, gloves, a hat, a scarf, boots, etc.</p>");

            answersArrayList.add("Autarchy with Queen ELIZABETH II as their Head of The State. In CANADA, the Governor-General carries out Her Majesty’s duties and is their de facto (in fact) Head of State.\n" +
                    "The Capital town of CANADA is OTTAWA, ONTARIO. Their Parliament consists of the House of Commons with 338 electoral members and senate, where 105 members are appointed. Members of parliament (MPs) are elected every 4 years on a mean. The Prime Minister who is generally the leader of the party with the foremost range of seats within the House of Commons is CANADA’S Head of Presidency. The Prime Minister appoints twenty to thirty ministers who form up the cabinet. The cabinet develops government policy and is accountable to the House of Commons.\n" +
                    "Under the leadership of premier, each of CANADA’S ten provinces and three territories contains a general assembly. Provincial and territorial governments are answerable for health care, education, driver’s license, social services, labor standards, and plenty of additional.\n" +
                    "Municipal and native governments are usually accountable and additionally plays a crucial role for urban or regional coming up with, streets and roads, snow removal, sanitation (such as garbage collection), firefighting services, emergency services, ambulance, recreational facilities, public transportation in addition to some native health and social services.\n");

            answersArrayList.add("<p>CANADA is one of the world&rsquo;s wealthiest major industrializedcountries. It&rsquo;s continually been a trading nation and therefore the engine of its economic process is its commerce. CANADA is a member of the G7, the G20, The Organization for Economic Co-operation and Development (OECD), The United Nations (UN), The World Trade Organization (WTO), and plenty of other international bodies.</p>\n" +
                    "<p><strong>Service Industries:</strong>&nbsp; It provides thousands of varied jobs in fields like transportation, health care, education, construction, banking, retail services, communications, tourism, and government. Quite seventy- fifths of operating Canadian&rsquo;s currently have jobs in Service industries.</p>\n" +
                    "<p><strong>Manufacturing Industries:</strong>&nbsp; CANADA&rsquo;S producing industries has vast potential for CANADA&rsquo;S economic future. The merchandise they create is oversubscribedwithin the CANADA and round the world. Factory-made merchandise embrace paper, high technological equipment, aerospace technology, automobiles, machinery, food, clothing, and many alternative goods. U.S. is the largest international mercantilism partner of CANADA.</p>\n" +
                    "<p><strong>Natural Resources Industries:</strong> These industries have contented a crucial and primary role in the country&rsquo;s history, and development. It includes biological science, fishing, agriculture, mining and energy. Several areas of the country still depend on developing natural resources in today&rsquo;s era. A giant share of CANADA&rsquo;S exports depends on its natural resources&rsquo;commodities. Despite of getting an extremely educated workforce, and a distributed economy, CANADA provides importance to its natural resources sector (e.g., forestry, mining &amp; energy, etc.)</p>\n" +
                    "<p>&nbsp;CANADA is the most technologically advanced and tech-savvy within the world and simultaneously it gives importance to its natural resources sector.</p>\n" +
                    "<p>Canadian Dollar is taken into account jointly as one of the foremost steady currencies within the world. All dollar figures given throughout this course are square measured Canadian Dollar (CAD).</p>");

            answersArrayList.add("<p>CANADA is a land of infinite opportunities. Over two hundred thousand international students opt for CANADA every year. It&rsquo;s because CANADA provides superb experiences to students around the world. Canadian Education is best renowned for its quality and is acknowledged for its excellence across the whole education sector moreover, as for the employers of labor all over the world. Over 90% of CANADA&rsquo;S university and college graduates are recruited in their field of study at an interval of 6 months after graduation. And concerning about 93 % of employers are happy with the graduates.</p>\n" +
                    "<p><strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </strong>CANADA is presently the #1 best country for quality of life and it&rsquo;s systematically hierarchal in a concert of the most effective countries within the world.&nbsp; By studying out in CANADA it opens all the gates for employment and one will receive an internationally recognized education from high educators and teachers in the world.</p>\n" +
                    "<p>Canadian&rsquo;s are popular in style for their entrepreneurial spirit, safe cities, best cultural talent, prime quality of life, and outstanding education in the world. This stuff makes CANADA a top-most selection for international language students. As per OECD, CANADA is best in terms of the standard educational system. Five out of the hundred best business colleges are in CANADA.</p>\n" +
                    "<p><strong><em>CANADA is multicultural and open to the world:</em></strong></p>\n" +
                    "<p>CANADA is home to over 200 languages and it&rsquo;s formally declared as a bilingual (English &amp; French) country. Several immigrants come to Canada&rsquo;s massive cities i.e. Vancouver, Toronto, and Montreal to live their dreams and one will realize all forms of ethnicities across the country. Canada&rsquo;s law and government&rsquo;s main concern is their Canadian values- they need a tolerant culture and it&rsquo;s thought of because the best-informally, on the streets and public venues, and formally in Canada&rsquo;s law and government. P.S: Charter of Rights and Freedoms that immortalize equality for all.</p>\n" +
                    "<p><strong><em>Canada is innovative: </em></strong></p>\n" +
                    "<p>CANADA is documented for its invention from a very long time. Its educational institutions are called the start-ups for innovation. The BlackBerry, flat- screen technology, sensible boards, voice compression applications for cell phones and computers, and IMAX films is among the various revolutionary technologies fancied, and developed by ladies, and men who studied in CANADA.</p>\n" +
                    "<p><strong><em>Canada rise leaders:</em></strong></p>\n" +
                    "<p>Among these are the reformist David Suzuki; the renowned designer Frank Gehry (Bilboa Guggenheim Museum, Walt Disney Concert Hall, etc.); the economic expert John Kenneth Galbraith ( who served in the administrations of US Presidents Franklin D. Roosevelt, Harry S. Truman, John F. Kennedy and Lyndon B. Johnson); the cinematographer James Cameron (Titanic, Avatar); the author Margaret Atwood; musicians Justin Bieber, Shawn Mendes and Sarah McLachlan; and Rachel McAdams, Jim Carrey, Kiefer Sutherland, and Ryan Gosling.</p>\n" +
                    "<p><strong><em>The standard of living as well as the cost to study are affordable:</em></strong></p>\n" +
                    "<p>The institutes in CANADA demand lower tuition fees for international students as compared to their equivalents in competitive countries and on the other hand, keep reaching to provide amazing educational qualities. Over the last 3 years, CANADA&rsquo;S rate of inflation has remained among rock bottomwithin the industrial world.</p>\n" +
                    "<p><strong><em>Work permit and permanent residency:</em></strong></p>\n" +
                    "<p>Overseas students can apply for their permanent residency which takes up to fifteen to eighteen months from within the CANADA. However, they are needed to fulfill the minimum eligibility criteria of 67 points. Whereas, studying in CANADA students are allowed to work part-time. The hours for work that has been started are up to 20 hours per week and on vacation, they can work full time. Students on the completion of their program of 1 year can work up to 1 year and students applying for 2 years program can work up to 3 years.</p>\n" +
                    "<p>In comparison to different countries, Canadian Immigration is one of the sorted and simplest immigrationsystems. Anybody who comes to CANADA is liberal to live and work across the country and confers upon that person a permanent resident standing. After 3 years an individual who is a Canadian PR will apply for its citizenship.</p>");

            answersArrayList.add("<p><strong><em>College and Vocational Courses in Canada- (Practical Learning).</em></strong></p>\n" +
                    "<p>By selecting to study at a Canadian college or vocational school one will gain valuable work expertise. College and vocational courses provide hand-on coaching, and teach the abilities employers are seeking. At the same time students will participate in internships, co-operative programs, and work placement that may provide real-world experience to students while they are studying.</p>\n" +
                    "<p><strong><em>Certificate Program:</em></strong>This program needs the completion of 2 semesters (or 1 year) of study, as approved by the Ministry of Training colleges and universities.</p>\n" +
                    "<p><strong><em>Diploma Program:</em></strong>It consists of 4 semesters (or two years) of study, as approved by the Ministry of Training, colleges and universities.</p>\n" +
                    "<p><strong><em>Advance Diploma:</em></strong>The program has 6 semesters (3 years) of study, as approved by the Ministryof Training, colleges and universities.</p>\n" +
                    "<p><strong><em>Graduate Certificate Program:</em></strong> It&rsquo;s co-jointly called Post Graduate Program that is usually of 1 year, however, it can even be for 2 years. A Post Graduate Credential is 120 credits whereas,a Master&rsquo;s degree is up to 180 credits. Among the number of foreign students, 70% of scholar students register themselves for Post Graduate Diploma.</p>\n" +
                    "<p>In short, Graduate certificate program is short centered, focused program designed to include advanced skills in a very specific subject or area of specialization to students. An enormous range of these programs are offered by documented colleges and universities, eachon-field and online. All of these open doors to sensible work experience and opportunities for the student&rsquo;s future.</p>\n" +
                    "<p>Overall, to study in CANADA is like a chance to open golden gates for your future.</p>");
        }

        else if (country.equals("aus")){

            questionsArrayList.add("| ABOUT AUSTRALIA");
            questionsArrayList.add("| WHY STUDY IN AUSTRALIA?");
            questionsArrayList.add("| EDUCATION SYSTEM IN AUSTRALIA");
            questionsArrayList.add("| AUSTRALIAN QUALIFICATIONS FRAMEWORK (AQF)");
            questionsArrayList.add("| STUDY LEVELS IN AUSTRALIA");

            answersArrayList.add("Continent wise AUSTRALIA is considered as the smallest but countrywide it is considered as one of the largest countries on earth. It lies in the Southern Hemisphere between the Indian ocean and thePacific Ocean. Canberra is the capital of Australia, which is located between the two main centers Sydney and Melbourne. With total 7,692,024 km sq. Australia is ranked at 6th position in terms of its total area. Australia is also called the Commonwealth of Australia. Its population comprises of 26 million people who are highly urbanized. Sydney is considered as the largest city in Australia as well as, it has also made its place in the world’s top richest cities list. The countries other metropolitan major cities are Melbourne, Perth, Adelaide, and Brisbane. \n" +
                    "Though Australia has no language as its official, English has been de facto national language since the European settlement. Australian English is very different from a wide pronunciation and lexicon, which slightly differs from other varieties of English in terms of Grammar and Spelling. The most common languages spoken after English are Mandarin, Arabic, Vietnamese, Cantonese, and so on.\n" +
                    "Australia although predominantly is a Christian country with around 52% of all Australians as Christians, but there is no official state religion. People residing there are free to practice any religion without outbreaking its law. With its huge cultural diversity, Australia is liberal to its residents and immigrants to worship any religion or no religion without offending their choice.\n" +
                    "Australia is very famous for its art since its prehistoric times. It includes colonial art, landscapes, painters from the early 20th century, sculptors, photographers, contemporary art, etc. Australia was home to many renowned artists such as Anita Aarons (sculptor), painter Harold Abbott, Louis Abraham, Tate Adams, and moreover.\n" +
                    "In short, foreign students in Australia who just can’t resist Art/Fine Arts will certainly have more to keep themselves busy during their stay.\n");

            answersArrayList.add("<p>When name Australia pop&rsquo;s up in people&rsquo;s minds they always see it as vast spaces of outback bushes, Koalas, Kangaroo, and pure air &amp; water. Australia has a diverse range of things to offer than just a common expectation. Australia is a famous study spot for students because of its cordial, friendly nature, magnificent education system, and a high standard of living.</p>\n" +
                    "<p>Below are the few reasons to opt Australia for education:</p>\n" +
                    "<ol>\n" +
                    "<li><strong><em>Growing Destination</em></strong><strong><em>:</em></strong></li>\n" +
                    "</ol>\n" +
                    "<p>Currently, Australia is considered as the 3<sup>rd</sup> most renowned destination for overseas students in the world of English-Speaking followed by the United Kingdom and the United States. Almost 70-80% of international students select Australia to study because of the diversified culture, cordial natives, and prime quality of education.</p>\n" +
                    "<ol start=\"2\">\n" +
                    "<li><strong><em>Global Acknowledgement:</em></strong></li>\n" +
                    "</ol>\n" +
                    "<p>Students who have graduated from Australian schools are extremely marketable because of their attractive universal reputation of the Australian education system. This system is delicately balanced by the government orderly, to prolong the high standards of education connected with the country.</p>\n" +
                    "<ol start=\"3\">\n" +
                    "<li><strong><em>Cost of living:</em></strong></li>\n" +
                    "</ol>\n" +
                    "<p>The standard of living is considered highest amidst the whole world. Tuition fees and cost of living are noticeably less in Australia as compared to the US and UK. Students are also allowed to work part-time off-campus while they study, accessing them to balance their living expenses. The Australian government also offers scholarships in the form of bursaries, compensation, etc. that can be helpful to overseas students to lower their studying costs. Around A$300 million is invested by the government of Australia for international students.</p>\n" +
                    "<ol start=\"4\">\n" +
                    "<li><strong><em>Diversified Education:</em></strong></li>\n" +
                    "</ol>\n" +
                    "<p>Australian institutes provide different variety of courses and degrees, so foreign students can be easily accessible to find the right school as per their educational stream and interest. The first and foremost thing foreign students should keep in mind before selecting a degree program is, which institution serves most to their interests and needs. Students will get an opportunity to choose between vocational education, universities, and English language training. If required, it will be easy for pupils to move between one qualification level to another and from one institution to another one.</p>\n" +
                    "<ol start=\"5\">\n" +
                    "<li><strong><em>Technology:</em></strong></li>\n" +
                    "</ol>\n" +
                    "<p>Australia&rsquo;s most fascinating feature for overseas students is the prominence of scientific research. Australia is at the cutting edge of the latest technology and innovations. Students who study in Australia can grab the opportunity of the country&rsquo;s attractive technology and resources.</p>\n" +
                    "<ol start=\"6\">\n" +
                    "<li><strong><em>Work:</em></strong></li>\n" +
                    "</ol>\n" +
                    "<p>During the study in Australia, students can work up to 20 hours per week. This can be very beneficial for the students who want to earn money to countervail living expenses until their stay, and also for students who want to acquire work experience in their interested field while their study.</p>");

            answersArrayList.add("<p>Australia is not appreciated only for its international education but also has significant chances to set up a universal network. Sincethe years, institutes of Australia have looked after the needs of its students by concentrating on actual learning and a thorough apprehension of various topics.</p>\n" +
                    "<p>Just like other countries, the Australian education system is mainly divided into 3 wide areas- Primary school, Secondary/Senior Secondary school, and Tertiary education.</p>\n" +
                    "<ol>\n" +
                    "<li>PRIMARY SCHOOL: Beginning from Kindergarten it goes for 7-8 years.</li>\n" +
                    "<li>SECONDARY SCHOOL: It sprints for 3 or 4 years from the age group of 7-10 or 8-10 years.</li>\n" +
                    "<li>SENIOR SCHOOL: It starts from the age group of 11 &amp; 12 years and continues for 2 years.</li>\n" +
                    "<li>TERTIARY EDUCATION: It comprises of both higher education and vocational education &amp; training (VET).</li>\n" +
                    "</ol>");

            answersArrayList.add("The special factor of the educational system in Australia is its execution of AQF, the 1995 national policy defines the standards of qualifications for the tertiary education sector additionally the school leaving certificate is called the senior secondary certificate of education. With the Australian Qualification Framework, student’s degree is accepted and permitted by the government.\n" +
                    "There are 10 levels under AQF, that conjoint different schools, colleges, universities, and educational institutions in one exclusive national system. This grants flexibility, option, and better career planning for pupils to budge from one level of study to another till their student visa requirements are satisfied.\n");

            answersArrayList.add("<p><strong><em>Australian study levels:</em></strong></p>\n" +
                    "<p>Australian educational system is three-layered with primary, secondary and tertiary levels of education. Below presented is an overview of the various levels and qualifications according to the AQF:</p>\n" +
                    "<p><strong><em>Levels and qualifications as per Australian qualifications framework.</em></strong></p>\n" +
                    "<p>LEVEL&nbsp; 1&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CERTIFICATE I</p>\n" +
                    "<p>LEVEL&nbsp; 2&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; CERTIFICATE II</p>\n" +
                    "<p>LEVEL&nbsp; 3&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; CERTIFICATE III</p>\n" +
                    "<p>LEVEL&nbsp; 4&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; CERTIFICATE IV</p>\n" +
                    "<p>LEVEL&nbsp; 5&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; DIPLOMA</p>\n" +
                    "<p>LEVEL&nbsp; 6&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ADVANCE DIPLOMA, ASSOCIATE DEGREE</p>\n" +
                    "<p>LEVEL&nbsp; 7&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; BACHELOR&rsquo;S DEGREE</p>\n" +
                    "<p>LEVEL&nbsp; 8&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; BACHELOR HONOURS DEGREE, GRADUATION CERTIFICATE,</p>\n" +
                    "<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; GRADUATION DIPLOMA</p>\n" +
                    "<p>LEVEL&nbsp; 9&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; MASTER&rsquo;S DEGREE</p>\n" +
                    "<p>LEVEL&nbsp; 10&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; DOCTORAL DEGREE</p>");

        }

        else if (country.equals("newZealand")){

            questionsArrayList.add("| ABOUT NEW ZEALAND");
            questionsArrayList.add("| WHY NEW ZEALAND?");
            questionsArrayList.add("| EDUCATION SYSTEM");

            answersArrayList.add("<p>The history of New Zealand is lavish, reflecting its special fusion of Maori and European culture. Maori people were the first to visit New Zealand, traveling in canoes from Hawaiki around 1000 years ago. First European who sighted New Zealand was a Dutchman, Abel Tasman but it was the British who made New Zealand as a part of their territory. People of New Zealand are very affectionate and realistic people who welcome the soul of manaakitanga, or hospitality. The national bird and national fruit of New Zealand is Kiwi. The fun fact about New Zealanders are, they are also called &lsquo;Kiwis&rsquo;. The population in New Zealand include people from various backgrounds; 70% of people are European Decent, 16.5% are indigenous Maori, 15% includes Asians, and 8.1% are Non-Maori Pacific Islanders. The main cities where New Zealanders dwell are Wellington, Christchurch, and Hamilton. The currency of New Zealand is NZD.</p>\n" +
                    "<p><strong><em>Climate and Weather</em></strong></p>\n" +
                    "<p>Weather in New Zealand is uncertain. The localities joke that one can undergo four seasons in one day! Far north area of New Zealand has subtropical weather. Whereas, the non-coastal alpine areas of the South Island can be as cold as -10`C in winter. Most of the country resided near to the coast, which means mild temperature around the year. July is the coldest month of the year while January &amp; February are the warmest months. One can enjoy the weather in New Zealand despite any season.</p>");

            answersArrayList.add("<p>New Zealand is renowned for its beauty, but despite its beauty, there&rsquo;s a lot more than that. It is becoming the topmost study destination for foreign students. New Zealand offers a brilliant education system, worldwide appraisal, vast research opportunities, and matchless quality education, New Zealand has a lot to offer. Below are some of the reasons why one should select New Zealand:</p>\n" +
                    "<p><strong><em>World recognized qualifications:</em></strong></p>\n" +
                    "<p>QS World Rankings and Times Higher Education ranked New Zealand&rsquo;s all 8 universities across the globe for prime quality education. New Zealand makes sure that its institutions are enrolled for the registration of overseas students to match their qualifications standards.</p>\n" +
                    "<p><strong><em>Innovation and Research:</em></strong></p>\n" +
                    "<p>The research opportunities are abundant for students in New Zealand. It boasts top faculties, high-tech laboratories and technology, and ample of opportunities. Its creativeness has made New Zealand an epi-center of latest technologies, research, and development in adifferent field such as physics, healthcare, geology, astronomy, engineering, agriculture, computer science, etc.</p>\n" +
                    "<p><strong><em>Great Opportunities for Ph.D. Scholars:</em></strong></p>\n" +
                    "<p>As a Ph.D. student one can take advantage of New Zealand&rsquo;s wide research opportunities. The tuition fee remains the same as the local students and alike other degree students.</p>\n" +
                    "<p><strong><em>Safe and peaceful:</em></strong></p>\n" +
                    "<p>Global Peace Index has ranked New Zealand 2<sup>nd</sup>. It has a very tolerant peaceful and stabilized society. New Zealand gives the international students same rights as their native counterparts.</p>\n" +
                    "<p><strong><em>Quality of Life:</em></strong></p>\n" +
                    "<p>Being a culturally diversified country New Zealand shares the same harmony with the country&rsquo;s calm &amp; stable lifestyle. Studying in New Zealand gives an advantage to nature lovers as it has all-natural landscapes such as snow-covered mountains, lavishing green hills golden sand beaches, lush rainforests, and steaming volcanoes.</p>\n" +
                    "<p><strong><em>Students Can Work to Offset Their Studies:</em></strong></p>\n" +
                    "<p>Students in New Zealand can work upto 20 hours per week during studies and can work full-time on holidays. If a student studying a doctoral degree and research masters can be allowed to work full-time.</p>");

            answersArrayList.add("<p>The education system in New Zealand motivates students to solve their problems, generate information, co-ordination with others, create, and innovate. Regardless of the level, you are studying, New Zealand promises students to give prime quality education to achieve their goals. The education system in New Zealand is a systematically leveled system which is divided into 10 levels. Those 10 levels are represented below:</p>\n" +
                    "<p>LEVEL 1, 2&amp;3 includes St. Secondary schools/Trades</p>\n" +
                    "<p>LEVEL 4 comprises different Certificate courses</p>\n" +
                    "<p>LEVEL 5&amp;6 includes Diploma courses offered after 12<sup>th</sup></p>\n" +
                    "<p>LEVEL 7 offers Bachelor/Graduate Diploma after the completion of graduation</p>\n" +
                    "<p>LEVEL 8 includes Bachelors with Honors/PG Diploma</p>\n" +
                    "<p>LEVEL 9 offer only aMaster&rsquo;s degree, and lastly</p>\n" +
                    "<p>LEVEL 10 comprises Research/PhD</p>\n" +
                    "<p>The major intakes are February, July, and November whereas some of the institutes, private colleges, and polytechnics have other passing years.</p>");

        }

        else if (country.equals("UK")){

            questionsArrayList.add("| ABOUT UNITED KINGDOM");
            questionsArrayList.add("| WHY STUDY IN UK?");
            questionsArrayList.add("| EDUCATION SYSTEM IN UK.");

            answersArrayList.add("The UNITED KINGDOM or (UK) is a political union made created from four countries, England, Wales, Scotland, and Northern Ireland. It’s truly known as UNITED KINGDOM of Great Britain(GB) and European Nation. Also,the UK conjointlyhas territories overseas like, islandand Gibraltor. Pound Sterling is that the currency of the United Kingdom.\n" +
                    "With the realm of 242,514 sq. km and 62.8 million population the UK stands at 21st position on rank population list. It’s capital and largest city is London that’s known for its beautiful ancient buildings, the palace of Westminister, Trafalgar Sq., London Eye, art galleries, and Big mount.\n" +
                    "Some of the most important cities in the UK are London, Birmingham, Manchester, Glasgow, Liverpool, etc. And every one of them has its due importance. English is the official and national language of the UK. The govt. in the United Kingdom is Unitary Parliamentary Constitutional Autarchy. Queen of England, Queen Elizabeth II is the Head of State. She became Britain’s longest- ruling monarch in Sept. 2015, continued the inheritance of her great-great- grandmother, Queen Victoria. She is also Head of State of alternative 16 freelance countries including Canada, Australia, and furthermore.Boris Johnson is the current Prime Minister of the UK. He became the Prime Minister in July 2019 once he defeated Jeremy Hunt in the race to become a frontrunner of the everyday party.\n" +
                    "The UK is understood for its altogether different cultures and folks of assorted religions reside there. It includes Christian, Non-Religious, Hindu, Muslim, Sikh, Jewish, Buddhist, etc. But amongst all, Christianity is claimed to be their major religion. However, they always welcome people with totally different religions with none offense.\n");

            answersArrayList.add("<p>When it involves education in the UK,it&rsquo;s one of the top destinations to study around the world. It has high category establishments on every corner, and with utmost flexibility than several alternative countries. With such a powerful legacy, education in the UK has become a benchmark for many countries.</p>\n" +
                    "<p>Below are a number of the explanations why Foreign students opt for the UK as their study destination:</p>\n" +
                    "<p><strong><em>Bewildering Academic Reputation:</em></strong></p>\n" +
                    "<p>The reputation of the UK around the world is at its peak, that no-one can deny and it is not burdensome to visualize why. Universities in the UK have an extended and previous history in Britain, with Oxford and Cambridge both each is supported over 800 years ago. British Universities have set their benchmark inside the world university league tables. In fact, around a fifth of the world&rsquo;s prime 50 universities are in the UK as per the QS world university rankings 2020!</p>\n" +
                    "<p><strong><em>Acknowledging Culture:</em></strong></p>\n" +
                    "<p>The UK could be a fusion of assorted cultures and with a tolerant society-in reality, individuals from varied parts of the globe choose UK, which students will notice.According to the Higher Education Statistic Agency (HESA), the UK is the 2<sup>nd</sup> most renowned and standard study destination within the world, with around 4,58,520 overseas students selecting to study at the UK&rsquo;s prime universities in 2017/18, thus one ought to be ready to receive a heart-warming welcome in the UK.</p>\n" +
                    "<p><strong><em>Wide-Ranging Choice for Degrees:</em></strong></p>\n" +
                    "<p>Universities in the UK are best illustrious for the broad range of degrees they provide in subjects as varied as Art &amp; Style, Business &amp; Management, and Media Studies. Students will realize a spectacular list of courses they&rsquo;re kneen for and, typically could even encounter a degree they didn&rsquo;t even know existed!</p>\n" +
                    "<p><strong><em>Shorter Degrees:</em></strong></p>\n" +
                    "<p>The undergraduate program in the UK is usually upto 3 years if studied full time.A master&rsquo;s degree typically takes 1 year. In distinction, to different countries like the USA, where at least a minimum of 4 years is required to completean undergraduate degree whereas, a master&rsquo;s degree includes 2 years to finish.</p>\n" +
                    "<p>So, if theUK is chosen to study, one should be ready to graduate shortly and to economize on fees and accommodation too. This can be indeed one of the simplest reasons to pick up the UK as a study destination.</p>\n" +
                    "<p><strong><em>Cultural Attractions:</em></strong></p>\n" +
                    "<p>The UK features an extended and engaging history. From Buckingham Palace and Tower of London to Edinburgh Castle, and Stonehenge; there&rsquo;s most to explore. If that doesn&rsquo;t excite you; the UK also has best concert venues like Birmingham&rsquo;s Symphony Hall, The Alexandra Palace in London, and some of the renowned music feasts around the world.</p>\n" +
                    "<p><strong><em>Largest Library Within the World:</em></strong></p>\n" +
                    "<p>London is known as the home to the biggest library within the world by numerous things listed- the British Library has over a hundred and seventy million things in its index!</p>\n" +
                    "<p>The British Library is found on the point of King&rsquo;s Cross terminus. It&rsquo;s a renownedplace to study quietly, and also, it&rsquo;sknown as home to the popular Reading Room. So, if students don&rsquo;t get their books in universities British library just about a guarantee to help you.</p>\n" +
                    "<p><strong><em>Improve Your English:</em></strong></p>\n" +
                    "<p>In this globalized world, English has become an integral part of one&rsquo;s progress and growth. And where will we have a bent to learn English better than in the UK?</p>\n" +
                    "<p>Since the student&rsquo;s class is going to be conducted in English, studying in the UK is the best way to boost your English proficiency.</p>\n" +
                    "<p><strong><em>High Standard:</em></strong></p>\n" +
                    "<p>The Final reason to study in the UK is that universities there, are inspected frequently by the Standard Assurance Agency (QAA) for higher instruction. This ensures that they maintain excellent standards in analysis, learning, and teaching. Teaching at universities in the UK is specially rated by the Teaching Excellence Framework (TEF) and research at institutions in the UK is ranked by the Research Excellence Framework(REF).These all appears that students have heaps of familiar choices to study at the United Kingdom.</p>");

            answersArrayList.add("<p>The UK is best known for its prime quality of education, interactive teaching method, high ranked universities, and student satisfaction. Degrees gained from the country are in demand globally preferred by employers across the world. The UK education system is divided into 4 main parts, primary education, secondary education, further education, and higher education.</p>\n" +
                    "<p><strong><em>Primary Education:</em></strong></p>\n" +
                    "<p>Primary education in the UK starts at the age of 5 and continues till the age of 11, including key stages 1 and 2 under the UK education system.</p>\n" +
                    "<p><strong><em>Secondary Education:</em></strong></p>\n" +
                    "<p>At the age of 11 to 16, students enter the secondary school for 3 and 4 key stages and to learn as well as to explore more about GCSES&rsquo;S- learn more about secondary education in the UK and what will it involve. Primary and secondary education until 16 is compulsory in the UK; after age 16 education in the UK is optional.</p>\n" +
                    "<p><strong><em>Further Education:</em></strong></p>\n" +
                    "<p>After the completion of secondary education, students have the option to go into further education to take their A-levels, BTEC&rsquo;S, GNVQ&rsquo;S, or many such qualifications. The UK students who plan to opt for college or university should complete their further education.</p>\n" +
                    "<p><strong><em>Higher Education:</em></strong></p>\n" +
                    "<p>In UK, higher education consists of bachelors, master&rsquo;s, and doctoral qualifications. As an overseas student, one can also transit to university level studies through pathway programs that usually include Foundation Courses and English Language Courses.</p>\n" +
                    "<p><strong><em>Undergraduate Courses:</em></strong></p>\n" +
                    "<p>These courses are intricately designed to help students to exhibit to a whole new world after school offering them a thorough knowledge of a subject of interest. The UK offers a range of courses in, art, social science, technology, and more.</p>\n" +
                    "<p><strong><em>Postgraduate Courses:</em></strong></p>\n" +
                    "<p>Postgraduate courses are in detail and more to the point. Most master&rsquo;s degrees offer 1- year course duration, which means one can be back to work fastly as compared to their equivalents opting for master&rsquo;s in their countries.</p>\n" +
                    "<p><strong><em>Major Intakes:</em></strong></p>\n" +
                    "<p>The UK academic season for universities starts in September or January.</p>\n" +
                    "<p>By studying in UK, one will be exposed to the ocean of excellence with great and numerous career opportunities.</p>");

        }

        else if (country.equals("US")){

            questionsArrayList.add("| ABOUT USA");
            questionsArrayList.add("| WHY THE USA?");
            questionsArrayList.add("| EDUCATION SYSTEM");

            answersArrayList.add("<p>The United States of America ranks 3<sup>rd</sup> largest in the terms of population and is also the 3<sup>rd</sup> largest country in size. It is also commonly called the USA. Washington, D.C. is the capital of the USA, which is a small city situated on the Potomac River. The calling code of the USA is +1. The population of the USA is around 329,256,465 with a total land area of 3,794,083 square miles. The currency which runs in the USA is the U.S. Dollar. Although the USA doesn&rsquo;t have any official national language English is considered as their official language followed by Spanish as their 2<sup>nd</sup> most spoken language.</p>\n" +
                    "<p>The major rivers of the USA are Mississippi, Missouri, and Colorado. There are more than 3.5 million miles of streams and rivers in the USA. The 5 Great Lakes that the USA shares with Canada are the prominent tract of freshwater on the planet and also a crucial part of America&rsquo;s physical &amp; cultural heritage. The largest Salt Lake in the Western Hemisphere with the area of 4,400 sq. km. is Utah&rsquo;s Great Salt Lake which is located in the USA. The USA comprises 4 seasons which people enjoy the most. Summer season starts from the last Monday in May and comes to an end on the first Monday in September. Usually, it is of 3-4 months with minimal temperature. Then comes Spring which begins from March and ends in May. Fall sprints from September to November and winter splashes from December to February. Fall and Winter season is generally of 3 months.</p>\n" +
                    "<p><strong><em>Population</em></strong></p>\n" +
                    "<p>The USA is the 3<sup>rd</sup> most populous country in the world, after China and India. The major cities of the USA with their respective population are as follows:</p>\n" +
                    "<p>New York - 8.4 million</p>\n" +
                    "<p>Los Angeles &ndash; 3.9 million</p>\n" +
                    "<p>Chicago &ndash; 2.7 million</p>\n" +
                    "<p>Houston &ndash; 2.7 million</p>\n" +
                    "<p><strong><em>Regions of USA</em></strong></p>\n" +
                    "<p>The USA is divided into 5 main areas:</p>\n" +
                    "<ol>\n" +
                    "<li>Northeast</li>\n" +
                    "<li>Southeast</li>\n" +
                    "<li>Midwest</li>\n" +
                    "<li>Southwest</li>\n" +
                    "<li>West</li>\n" +
                    "</ol>\n" +
                    "<p>Each area has its due importance. Below are some of the details regarding each above- mentioned regions:</p>\n" +
                    "<ol>\n" +
                    "<li><strong>Northeast</strong></li>\n" +
                    "</ol>\n" +
                    "<p>The climate generally in this region is a humid steamy climate with refreshing summers in the northernmost area. The winters generally are below freezing with snowfall.</p>\n" +
                    "<p>The states included are Maine, Massachusetts, Rhode Island, New Jersey, Connecticut, New York, Pennsylvania, New Hemisphere, Delaware, Maryland.</p>\n" +
                    "<ol start=\"2\">\n" +
                    "<li><strong>Southeast</strong></li>\n" +
                    "</ol>\n" +
                    "<p>This region has a Semitropical humid climate with peppery summers season. West Virginia, Tennessee, Virginia, Kentucky, North-South Carolina, Alabama, Georgia, Mississippi, Arkansas, Louisiana, Florida are states included in this region.</p>\n" +
                    "<ol start=\"3\">\n" +
                    "<li><strong>Midwest</strong></li>\n" +
                    "</ol>\n" +
                    "<p>Here the climate steamy continental all over the region. Snowfall is considered very usual during winters, mainly in the northern parts.</p>\n" +
                    "<p>States incorporated in this region are Ohio, North Dakota, Minnesota, Lowa, Nebraska, Kansas, South Dakota, Indiana, Winconsin, Michigan, Missouri, Illinois.</p>\n" +
                    "<ol start=\"4\">\n" +
                    "<li><strong>Southwest</strong></li>\n" +
                    "</ol>\n" +
                    "<p>There are only 4 states included in this region, Texas, Oklahoma, Arizona, and New Mexico.</p>\n" +
                    "<p>The climate in its east is quite humid while the climate in the western area is semi-arid veld climate. Several distant western areas of this region comprise an alpine or desert climate.</p>\n" +
                    "<p>&nbsp;</p>\n" +
                    "<ol start=\"5\">\n" +
                    "<li><strong>West</strong></li>\n" +
                    "</ol>\n" +
                    "<p>A mixture of climates including alpine and semi- arid alongside the Sierra and the Rocky Mountains. There are 2 states here in which one can find deserts- like climate, Nevada, and Southern California.</p>\n" +
                    "<p>Colorado, Montana, Idaho, Wyoming, Oregon, Washington, Nevada, Utah, California, Hawaii, and Alaska.</p>\n" +
                    "<p><strong><em>&nbsp;</em></strong></p>\n" +
                    "<p><strong><em>Religion and Culture</em></strong></p>\n" +
                    "<p>The United States is considered as one of the most culturally diverse countries. Almost every region across the globe has been influenced by its culture. The culture in the USA is an amalgamation of the cultures of Native America, Africans, Latin Americans, and Asians. The USA is impressively called as &lsquo;melting pot&rsquo; in which the various flavors are incorporated to create a magnificent dish. The Same way the people from other countries had so beautifully involved themselves in its culture that sometimes it is difficult to differentiate. They USA strongly believes in individualism which means every person has the right to practice their own culture and religion without any offense.&nbsp; Almost every religion is practiced in the United States, as it believes in religious freedom. Around 71% of Americans have identified to be Christians while 23% of the people are non-religious and about 6% of the population comprises non-Christian religion.</p>\n" +
                    "<p><strong><em>Government and Economy</em></strong></p>\n" +
                    "<p>The United States is the 2<sup>nd</sup> largest trading land in the world after China. It is considered as a free-market economy, where individuals and agencies buy their technology and the economy is regulated by self-reliant transactions between purchasers and retailers more than 20% of the world&rsquo;s total income Canada, China, Japan, Germany, and Mexico are its largest trading partners. US dollar is considered the most popular currency.</p>\n" +
                    "<p>Natives who are over 18 years have the right to vote and elect the president &amp; vice president. The person who gets elected as a president lives in the White House in the capital of Washington, D.C. The election is held every 4 years. Barack Obama was elected as the president in 2008 and also he was reelected in 2012 for a second term. Donald Trump is currently, the president of the USA, he is the 45<sup>th</sup> USA president. Donald Trump won he 2016 elections and was elected on 9<sup>th</sup> November. Mike Pence is the vice president of the USA.</p>");

            answersArrayList.add("<p>The United States introduces the greatest number of overseas students. There are various reasons why students choose the USA as their study destination. It is renowned for its standard education, special syllabus, diversified environment, and ample of opportunities are some reasons why international students opt for the USA. Amongst them Few reasons are mentioned below:</p>\n" +
                    "<p><strong><em>Academic Greatness:</em></strong></p>\n" +
                    "<p>&nbsp;The US has some of the excellent universities, amongst them, many universities are normally ranked in the world university rankings. American institutes are renowned for their prime academic standards accompanied carefully to preserve quality and are skillfully-supported to provide the best education to its students.</p>\n" +
                    "<p>&nbsp;</p>\n" +
                    "<p><strong><em>Malleable Education System:</em></strong></p>\n" +
                    "<p>The universities in the USA provide various programs to its students to choose from. Students have liberty to choose the content of their course as well as they can also choose the structure. Students must choose courses at the undergraduate level before the announcement of the second year. This helps students to explore their subject of interest and then to make a decision without a hurry. Simultaneously, for graduate studies, students can select their choices and can concentrate on the ideas they want to force upon.</p>\n" +
                    "<p>&nbsp;</p>\n" +
                    "<p><strong><em>Excellent Support System for Foreign Students:</em></strong></p>\n" +
                    "<p>The obstacles which foreign students pass through are immense but American universities understand their struggles and therefore organize day-to-day orientation programs, workshops, and training to assist. The local students help the foreign pupils to get accustomed to a new lifestyle, even they will assist the students with any academic, cultural or, social queries.</p>\n" +
                    "<p>&nbsp;</p>\n" +
                    "<p><strong><em>Cultural Diversity:</em></strong></p>\n" +
                    "<p>The USA is known as a melting pot of various cultures, religions, ethnicities. Its acceptable environment supports every community without any discrimination. Students all over the world make it an experience worth remembering and one can also learn from it. Currently, employers prefer students with a fusion of various cultures which one could easily get in the USA. The USA will help to explore many famous dishes from around the world, customs, festivals, and art too.</p>\n" +
                    "<p>&nbsp;</p>\n" +
                    "<p><strong><em>Active and Spirited Campus Life:</em></strong></p>\n" +
                    "<p>It&rsquo;s a well- known reality that the campus life of the USAis incomparable. Regardless of the university student&rsquo;s study, they will experience a flavor of a new culture and life in American style.</p>");

            answersArrayList.add("<p>To study in America one can, have heart-throbbing experiences and a vast range of study options for graduates and undergraduates from across the world.</p>\n" +
                    "<p><strong><em>TYPES OF COURSES IN USA</em></strong></p>\n" +
                    "<p>UNDERGRADUATE AT A UNIVERSITY/COLLEGE:&nbsp; Bachelor&rsquo;s degree is usually of 3-4 years. Each year of college has a specific word to identify the student&rsquo;s academic standing.</p>\n" +
                    "<p>1<sup>ST</sup> YEAR- Freshman</p>\n" +
                    "<p>2<sup>ND </sup>YEAR- Sophomore</p>\n" +
                    "<p>3<sup>RD</sup> YEAR- JUNIOR</p>\n" +
                    "<p>4<sup>TH</sup> YEAR- SENIOR</p>\n" +
                    "<p>As soon as they complete their curriculum, they are asked to select a particular area of study also called as final. Bachelor&rsquo;s degree program varies from 120 credit hours to 150 hours depending on the credit hours needed.</p>\n" +
                    "<p>MATERS:&nbsp; After the completion of bachelor&rsquo;s degree students can proceed for master&rsquo;s degree with essential grade and experience required. Master&rsquo;s degree generally comprises 1-3 years of course and completion most of them also includes research.</p>\n" +
                    "<p>Usually a master&rsquo;s degree program extent between 30 &amp; 60 hours of credit depending on the hours required, the average need is 36.</p>\n" +
                    "<p>DOCTORAL DEGRESS:&nbsp; This degree is considered as academic and executive in nature. To attain their degrees, it is mandatory for students to pass an exam and to complete a research which needs to be original and submit a dissertation. Doctoral degrees need 5-8 years to finish.</p>");

        }

    }

}
