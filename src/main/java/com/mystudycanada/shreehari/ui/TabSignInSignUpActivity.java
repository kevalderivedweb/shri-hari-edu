package com.mystudycanada.shreehari.ui;

import android.content.Context;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

public class TabSignInSignUpActivity extends FragmentPagerAdapter {

    private Context myContext;
    private int totalTabs;

    public TabSignInSignUpActivity(Context context, FragmentManager fm, int totalTabs) {
        super(fm);
        myContext = context;
        this.totalTabs = totalTabs;
    }

    // this is for fragment tabs
    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                FragmentTestLitening fragmentTestFormat = new FragmentTestLitening();
                return fragmentTestFormat;
            case 1:
                FragmentTestLitening fragmentTestFormat1 = new FragmentTestLitening();
                /*Bundle bundle = new Bundle();
                bundle.putString("city_id", mCityId);
                signUpActivity.setArguments(bundle);*/
                return fragmentTestFormat1;
            case 2:
                FragmentTestLitening fragmentTestFormat2 = new FragmentTestLitening();
                return fragmentTestFormat2;

            default:
                return null;
        }
    }


    // this counts total number of tabs
    @Override
    public int getCount() {
        return totalTabs;
    }
}
