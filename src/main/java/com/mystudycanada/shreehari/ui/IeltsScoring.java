package com.mystudycanada.shreehari.ui;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import com.mystudycanada.shreehari.Adapter.ExpListAdapterFrequentAsked;
import com.mystudycanada.shreehari.R;

import java.util.ArrayList;

public class IeltsScoring extends Fragment {

    private ExpandableListView expandableListView;
    private ExpListAdapterFrequentAsked expListAdapterFrequentAsked;

    private ArrayList<String> headerArray = new ArrayList<>();
    private ArrayList<String> descriArray = new ArrayList<>();


    String html = "<table>\n" +
            "<tbody>\n" +
            "<tr>\n" +
            "<th class=\"heading\"><center>Band Score</center></th>\n" +
            "<th class=\"heading\"><center>Skill Level</center></th>\n" +
            "<th class=\"heading\"><center>Band Description</center></th>\n" +
            "</tr>\n" +
            "<tr>\n" +
            "<td class=\"table_td\">9</td>\n" +
            "<td class=\"table_td\">Expert user</td>\n" +
            "<td class=\"table_td\">Have fully operational command of the language: appropriate, accurate and fluentwith complete understanding.</td>\n" +
            "</tr>\n" +
            "<tr>\n" +
            "<td class=\"table_td\">8</td>\n" +
            "<td class=\"table_td\">Very good User</td>\n" +
            "<td class=\"table_td\">Have fully operational command of the language with only occasional unsystematic inaccuracies and inappropriate. Misunderstandings may occur.In unfamiliar situations. Handles complex detailed arguments well.</td>\n" +
            "</tr>\n" +
            "<tr>\n" +
            "<td class=\"table_td\">7</td>\n" +
            "<td class=\"table_td\">Good User</td>\n" +
            "<td class=\"table_td\">Have operational command of the language, though with occasionalInaccuracies, inappropriate, and misunderstandings in some situations. Generally,handles complex language well and understands detailed reasoning.</td>\n" +
            "</tr>\n" +
            "<tr>\n" +
            "<td class=\"table_td\">6</td>\n" +
            "<td class=\"table_td\">Competent User</td>\n" +
            "<td class=\"table_td\">Has generally effective command of the language despite someinaccuracies, inappropriacy and misunderstandings. Can use and understand fairlycomplex language, particularly in familiar situations.</td>\n" +
            "</tr>\n" +
            "<tr>\n" +
            "<td class=\"table_td\">5</td>\n" +
            "<td class=\"table_td\">Modest User</td>\n" +
            "<td class=\"table_td\">Has partial command of the language, coping with overall meaning inmost situations, though is likely to make many mistakes. Should be able to handle basic communication in own field.</td>\n" +
            "</tr>\n" +
            "<tr>\n" +
            "<td class=\"table_td\">4</td>\n" +
            "<td class=\"table_td\">Limited User</td>\n" +
            "<td class=\"table_td\">Basic competence is limited to familiar situations. Has frequentproblems in understanding and expression. Is not able to use complex language.</td>\n" +
            "</tr>\n" +
            "<tr>\n" +
            "<td class=\"table_td\">3</td>\n" +
            "<td class=\"table_td\">Extremely Limited User</td>\n" +
            "<td class=\"table_td\">Conveys and understands only general meaning invery familiar situations. Frequent breakdowns in communication occur.</td>\n" +
            "</tr>\n" +
            "<tr>\n" +
            "<td class=\"table_td\">2</td>\n" +
            "<td class=\"table_td\">Intermittent User</td>\n" +
            "<td class=\"table_td\">No real communication is possible except for the most basicinformation using isolated words or short formulae in familiar situations and to meetimmediate needs. Has great difficulty understanding spoken and written English.</td>\n" +
            "</tr>\n" +
            "<tr>\n" +
            "<td class=\"table_td\">1</td>\n" +
            "<td class=\"table_td\">Non-User</td>\n" +
            "<td class=\"table_td\">Essentially has no ability to use the language beyond possibly a fewisolated words.</td>\n" +
            "</tr>\n" +
            "<tr>\n" +
            "<td class=\"table_td\">0</td>\n" +
            "<td class=\"table_td\">Did Not Attempt the Test</td>\n" +
            "<td class=\"table_td\">No assessable information provided.</td>\n" +
            "</tr>\n" +
            "</tbody>\n" +
            "</table>";


    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_ielts_scoring, container, false);

        expandableListView = view.findViewById(R.id.lvExpListScoring);
        expandableListView.setChildDivider(getResources().getDrawable(R.color.white));


        expListAdapterFrequentAsked = new ExpListAdapterFrequentAsked(getActivity(), headerArray, descriArray);
        // setting list adapter
        expandableListView.setAdapter(expListAdapterFrequentAsked);
        // Listview Group click listener
        expandableListView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {

            @Override
            public boolean onGroupClick(ExpandableListView parent, View v,
                                        int groupPosition, long id) {
                // Toast.makeText(getApplicationContext(),
                // "Group Clicked " + listDataHeader.get(groupPosition),
                // Toast.LENGTH_SHORT).show();

               /* if (groupPosition == 4){
                    Dialog dialogForCity = new Dialog(getContext());
                    dialogForCity.setContentView(R.layout.custom_dialog_city);
                    dialogForCity.setCancelable(true);
                    dialogForCity.setCanceledOnTouchOutside(true);
                    dialogForCity.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                    Window window = dialogForCity.getWindow();
                    window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);

                  *//*  webView = dialogForCity.findViewById(R.id.webView);

                    webView.loadDataWithBaseURL(null, html, "text/html", "utf-8", null);
*//*



                    dialogForCity.show();
                }*/

                setListViewHeight(parent, groupPosition);
                return false;
            }
        });


        testArrayData();

        expListAdapterFrequentAsked.notifyDataSetChanged();

        // Listview on child click listener
        expandableListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {

            @Override
            public boolean onChildClick(ExpandableListView parent, View v,
                                        int groupPosition, int childPosition, long id) {
                // TODO Auto-generated method stub

                return false;
            }
        });


        view.findViewById(R.id.back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //   getFragmentManager().popBackStack();

                FragmentManager fm = getFragmentManager();
                if (fm.getBackStackEntryCount() > 0) {
                    Log.i("MainActivity", "popping backstack");
                    fm.popBackStack();
                } else {
                    Log.i("MainActivity", "nothing on backstack, calling super");
                }

            }
        });

        return view;

    }


    private void setListViewHeight(ExpandableListView listView, int group) {
        ExpandableListAdapter listAdapter = (ExpandableListAdapter) listView.getExpandableListAdapter();
        int totalHeight = 0;
        int desiredWidth = View.MeasureSpec.makeMeasureSpec(listView.getWidth(),
                View.MeasureSpec.EXACTLY);
        for (int i = 0; i < listAdapter.getGroupCount(); i++) {
            View groupItem = listAdapter.getGroupView(i, false, null, listView);
            groupItem.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);

            totalHeight += groupItem.getMeasuredHeight();

            if (((listView.isGroupExpanded(i)) && (i != group))
                    || ((!listView.isGroupExpanded(i)) && (i == group))) {
                for (int j = 0; j < listAdapter.getChildrenCount(i); j++) {
                    View listItem = listAdapter.getChildView(i, j, false, null,
                            listView);
                    listItem.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);

                    totalHeight += listItem.getMeasuredHeight();

                }
                //Add Divider Height
                totalHeight += listView.getDividerHeight() * (listAdapter.getChildrenCount(i) - 1);
            }
        }
        //Add Divider Height
        totalHeight += listView.getDividerHeight() * (listAdapter.getGroupCount() - 1);

        ViewGroup.LayoutParams params = listView.getLayoutParams();
        int height = totalHeight
                + (listView.getDividerHeight() * (listAdapter.getGroupCount() - 1));
        if (height < 10)
            height = 200;
        params.height = height;
        listView.setLayoutParams(params);
        listView.requestLayout();
    }


    private void testArrayData() {

        headerArray.add("What Is a Band Score?");
        headerArray.add("How are the scores calculated?");
        headerArray.add("Scoring in different sections");
        headerArray.add("What is a Good IELTS Score?");
        headerArray.add("IELTS 9-BAND SCALE:");


        descriArray.add("<p>IELTS scores on a scale from 1 (lowest) to 9 (highest) are recorded as band scores. For each test section, you&rsquo;ll be given a band score from 1 to 9.</p>\n" +
                "<p>Listening, Reading, Writing and Speaking. You&rsquo;ll also get an overall band score which is the average of your four individual band scores in the test segment.</p>");


        descriArray.add("<p>All the scores of the test are between 0 to 9. The scores are calculated by averaging outthe scores of the above mentioned four sections. The scores can be .5 as well as 6.5 or7.5. In the case of decimal figures other than 5 or 0, the score is rounded up or down tothe nearest 0.5. For instance, 6.1 will be rounded down to 6 or 7.75 will be rounded upto 8.</p>\n" +
                "<p>Each score has been given a unique description and is associated with a skill level. Forexample, &lsquo;9&rsquo; denotes an expert user whereas &lsquo;0&rsquo; denotes no attempt. Scores from 9 to 6depicts that one understands the basics of the language, whereas 5 to 0 depicts thatone has limited or no knowledge about the language.</p>");


        descriArray.add("<p>In Reading and Listening sections, with 40 questions each, one can score a maximumof 40 marks in each section. The marks awarded are called raw scores which areconverted to the band score as per the makers of IELTS give the directions.For Speaking and Writing sections, various band descriptions have been provided. Theaverage score of these descriptions will be the score in these sections. Fluency andcoherence, Lexical resource, Grammar, Pronunciation are the parameters forSpeakingwhereas Task achievement, Cohesion along with Grammar and Lexical resource arethe band descriptions for Writing.</p>");


        descriArray.add("<p>A good academic score is required for admission to different foreign universities. Now, the question of what is defined as &ldquo;good&rdquo; arises. Well, it differs from country to country and university to university. If you are considering studying abroad or pursuing opportunities in Canada or other countries, then you need a minimum overall score of 6.5, with a minimum score of 6.0 in each category for the Academic test. Minimum accepted scores vary depending on the country and institution requirements.</p>");

        descriArray.add("");

      /*  descriArray.add("<table>\n" +
                "<tbody>\n" +
                "<tr>\n" +
                "<th class=\"heading\"><center>Band Score</center></th>\n" +
                "<th class=\"heading\"><center>Skill Level</center></th>\n" +
                "<th class=\"heading\"><center>Band Description</center></th>\n" +
                "</tr>\n" +
                "<tr>\n" +
                "<td class=\"table_td\">9</td>\n" +
                "<td class=\"table_td\">Expert user</td>\n" +
                "<td class=\"table_td\">Have fully operational command of the language: appropriate, accurate and fluentwith complete understanding.</td>\n" +
                "</tr>\n" +
                "<tr>\n" +
                "<td class=\"table_td\">8</td>\n" +
                "<td class=\"table_td\">Very good User</td>\n" +
                "<td class=\"table_td\">Have fully operational command of the language with only occasional unsystematic inaccuracies and inappropriate. Misunderstandings may occur.In unfamiliar situations. Handles complex detailed arguments well.</td>\n" +
                "</tr>\n" +
                "<tr>\n" +
                "<td class=\"table_td\">7</td>\n" +
                "<td class=\"table_td\">Good User</td>\n" +
                "<td class=\"table_td\">Have operational command of the language, though with occasionalInaccuracies, inappropriate, and misunderstandings in some situations. Generally,handles complex language well and understands detailed reasoning.</td>\n" +
                "</tr>\n" +
                "<tr>\n" +
                "<td class=\"table_td\">6</td>\n" +
                "<td class=\"table_td\">Competent User</td>\n" +
                "<td class=\"table_td\">Has generally effective command of the language despite someinaccuracies, inappropriacy and misunderstandings. Can use and understand fairlycomplex language, particularly in familiar situations.</td>\n" +
                "</tr>\n" +
                "<tr>\n" +
                "<td class=\"table_td\">5</td>\n" +
                "<td class=\"table_td\">Modest User</td>\n" +
                "<td class=\"table_td\">Has partial command of the language, coping with overall meaning inmost situations, though is likely to make many mistakes. Should be able to handle basic communication in own field.</td>\n" +
                "</tr>\n" +
                "<tr>\n" +
                "<td class=\"table_td\">4</td>\n" +
                "<td class=\"table_td\">Limited User</td>\n" +
                "<td class=\"table_td\">Basic competence is limited to familiar situations. Has frequentproblems in understanding and expression. Is not able to use complex language.</td>\n" +
                "</tr>\n" +
                "<tr>\n" +
                "<td class=\"table_td\">3</td>\n" +
                "<td class=\"table_td\">Extremely Limited User</td>\n" +
                "<td class=\"table_td\">Conveys and understands only general meaning invery familiar situations. Frequent breakdowns in communication occur.</td>\n" +
                "</tr>\n" +
                "<tr>\n" +
                "<td class=\"table_td\">2</td>\n" +
                "<td class=\"table_td\">Intermittent User</td>\n" +
                "<td class=\"table_td\">No real communication is possible except for the most basicinformation using isolated words or short formulae in familiar situations and to meetimmediate needs. Has great difficulty understanding spoken and written English.</td>\n" +
                "</tr>\n" +
                "<tr>\n" +
                "<td class=\"table_td\">1</td>\n" +
                "<td class=\"table_td\">Non-User</td>\n" +
                "<td class=\"table_td\">Essentially has no ability to use the language beyond possibly a fewisolated words.</td>\n" +
                "</tr>\n" +
                "<tr>\n" +
                "<td class=\"table_td\">0</td>\n" +
                "<td class=\"table_td\">Did Not Attempt the Test</td>\n" +
                "<td class=\"table_td\">No assessable information provided.</td>\n" +
                "</tr>\n" +
                "</tbody>\n" +
                "</table>");*/

    }


}

