package com.mystudycanada.shreehari.ui;

import android.graphics.Color;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabLayout;
import com.mystudycanada.shreehari.Adapter.ExpListAdapterFrequentAsked;
import com.mystudycanada.shreehari.R;

import java.util.ArrayList;

public class IeltsTest extends Fragment {

    private TextView testFormatText;
   /* private TabLayout tabLayoutSignInUp;
    private ViewPager viewPagerSignInUp;*/

    private ExpandableListView expandableListView;
    private ExpListAdapterFrequentAsked expListAdapterFrequentAsked;

    private ArrayList<String> headerArray = new ArrayList<>();
    private ArrayList<String> descriArray = new ArrayList<>();

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_ielts_test, container, false);

        testFormatText = view.findViewById(R.id.testFormatText);

        expandableListView = view.findViewById(R.id.lvExpListTest);
        expandableListView.setChildDivider(getResources().getDrawable(R.color.white));


        expListAdapterFrequentAsked = new ExpListAdapterFrequentAsked(getActivity(), headerArray, descriArray);
        // setting list adapter
        expandableListView.setAdapter(expListAdapterFrequentAsked);
        // Listview Group click listener
        expandableListView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {

            @Override
            public boolean onGroupClick(ExpandableListView parent, View v,
                                        int groupPosition, long id) {
                // Toast.makeText(getApplicationContext(),
                // "Group Clicked " + listDataHeader.get(groupPosition),
                // Toast.LENGTH_LONG).show();

                setListViewHeight(parent, groupPosition);
                return false;
            }
        });



        testArrayData();

        expListAdapterFrequentAsked.notifyDataSetChanged();

        // Listview on child click listener
        expandableListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {

            @Override
            public boolean onChildClick(ExpandableListView parent, View v,
                                        int groupPosition, int childPosition, long id) {
                // TODO Auto-generated method stub

                return false;
            }
        });


        view.findViewById(R.id.back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //   getFragmentManager().popBackStack();

                FragmentManager fm = getFragmentManager();
                if (fm.getBackStackEntryCount() > 0) {
                    Log.i("MainActivity", "popping backstack");
                    fm.popBackStack();
                } else {
                    Log.i("MainActivity", "nothing on backstack, calling super");
                }

            }
        });

        testFormatText.setText(Html.fromHtml("<div class=\"elementor-element elementor-element-7c49f51 elementor-widget elementor-widget-text-editor\" data-id=\"7c49f51\" data-element_type=\"widget\" data-widget_type=\"text-editor.default\">\n" +
                "<div class=\"elementor-widget-container\">\n" +
                "<div class=\"elementor-text-editor elementor-clearfix\">\n" +
                "<div class=\"elementor-element elementor-element-7c49f51 elementor-widget elementor-widget-text-editor\" data-id=\"7c49f51\" data-element_type=\"widget\" data-widget_type=\"text-editor.default\">\n" +
                "<div class=\"elementor-widget-container\">\n" +
                "<div class=\"elementor-text-editor elementor-clearfix\">\n" +
                "<div class=\"elementor-element elementor-element-6594087 elementor-icon-list--layout-traditional elementor-widget elementor-widget-icon-list\" data-id=\"6594087\" data-element_type=\"widget\" data-widget_type=\"icon-list.default\">\n" +
                "<div class=\"elementor-widget-container\">\n" +
                "<ul class=\"elementor-icon-list-items\">\n" +
                "> The IELTS test assesses candidate&rsquo;s abilities of Listening, Reading, Writing, and Speaking English.</span></li>\n" +
                "</ul>\n" +
                "</div>\n" +
                "</div>\n" +
                "<div class=\"elementor-element elementor-element-169524e elementor-icon-list--layout-traditional elementor-widget elementor-widget-icon-list\" data-id=\"169524e\" data-element_type=\"widget\" data-widget_type=\"icon-list.default\">\n" +
                "<div class=\"elementor-widget-container\">\n" +
                "<ul class=\"elementor-icon-list-items\">\n" +
                "> There are two modules of the IELTS test: Academic and General Training.</span></li>\n" +
                "</ul>\n" +
                "</div>\n" +
                "</div>\n" +
                "<div class=\"elementor-element elementor-element-2e36621 elementor-icon-list--layout-traditional elementor-widget elementor-widget-icon-list\" data-id=\"2e36621\" data-element_type=\"widget\" data-widget_type=\"icon-list.default\">\n" +
                "<div class=\"elementor-widget-container\">\n" +
                "<ul class=\"elementor-icon-list-items\">\n" +
                "> Listening and speaking are the same for both the modules. But the formats of the Reading and Writing section of both the tests are different.</span></li>\n" +
                "</ul>\n" +
                "</div>\n" +
                "</div>\n" +
                "<div class=\"elementor-element elementor-element-d824158 elementor-icon-list--layout-traditional elementor-widget elementor-widget-icon-list\" data-id=\"d824158\" data-element_type=\"widget\" data-widget_type=\"icon-list.default\">\n" +
                "<div class=\"elementor-widget-container\">\n" +
                "<ul class=\"elementor-icon-list-items\">\n" +
                "> The Listening, Reading, and Writing section is taken on the same day, with no break. The Speaking test, however, is one week before the test date or after the test date.</span></li>\n" +
                "</ul>\n" +
                "</div>\n" +
                "</div>\n" +
                "<div class=\"elementor-element elementor-element-2551d00 elementor-icon-list--layout-traditional elementor-widget elementor-widget-icon-list\" data-id=\"2551d00\" data-element_type=\"widget\" data-widget_type=\"icon-list.default\">\n" +
                "<div class=\"elementor-widget-container\">\n" +
                "<ul class=\"elementor-icon-list-items\">\n" +
                "> LRW (Listening, Reading, and Writing) is of 2 hours and 45 minutes.</span></li>\n" +
                "</ul>\n" +
                "</div>\n" +
                "</div>\n" +
                "<div class=\"elementor-element elementor-element-f6f71af elementor-icon-list--layout-traditional elementor-widget elementor-widget-icon-list\" data-id=\"f6f71af\" data-element_type=\"widget\" data-widget_type=\"icon-list.default\">\n" +
                "<div class=\"elementor-widget-container\">\n" +
                "<ul class=\"elementor-icon-list-items\">\n" +
                "> Speaking test lasts for 11-14 minutes.</span></li>\n" +
                "</ul>\n" +
                "</div>\n" +
                "</div>\n" +
                "</div>\n" +
                "</div>\n" +
                "</div>\n" +
                "</div>\n" +
                "</div>\n" +
                "</div>"));


        /*tabLayoutSignInUp=(TabLayout)view.findViewById(R.id.tabLayoutSignInUp);
        viewPagerSignInUp=(ViewPager)view.findViewById(R.id.viewPagerSignInUp);

        tabLayoutSignInUp.setSelectedTabIndicatorColor(Color.parseColor("#3c5bc3"));
        tabLayoutSignInUp.setSelectedTabIndicatorHeight((int) (1 * getResources().getDisplayMetrics().density));
        tabLayoutSignInUp.setTabTextColors(Color.parseColor("#6d6d6d"), Color.parseColor("#3c5bc3"));

        tabLayoutSignInUp.addTab(tabLayoutSignInUp.newTab().setText("Listening"));
        tabLayoutSignInUp.addTab(tabLayoutSignInUp.newTab().setText("Academic Reading"));
        tabLayoutSignInUp.addTab(tabLayoutSignInUp.newTab().setText("General Reading"));
        tabLayoutSignInUp.addTab(tabLayoutSignInUp.newTab().setText("Academic writing"));
        tabLayoutSignInUp.addTab(tabLayoutSignInUp.newTab().setText("General writing"));
        tabLayoutSignInUp.addTab(tabLayoutSignInUp.newTab().setText("Speaking"));

        tabLayoutSignInUp.setTabGravity(TabLayout.GRAVITY_FILL);

        final TabSignInSignUpActivity pagerTabActivity =
                new TabSignInSignUpActivity(getActivity(), getFragmentManager(), tabLayoutSignInUp.getTabCount());
        viewPagerSignInUp.setAdapter(pagerTabActivity);


        viewPagerSignInUp.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayoutSignInUp));



        tabLayoutSignInUp.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPagerSignInUp.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });*/


        return view;
    }

    private void setListViewHeight(ExpandableListView listView, int group) {
        ExpandableListAdapter listAdapter = (ExpandableListAdapter) listView.getExpandableListAdapter();
        int totalHeight = 0;
        int desiredWidth = View.MeasureSpec.makeMeasureSpec(listView.getWidth(),
                View.MeasureSpec.EXACTLY);
        for (int i = 0; i < listAdapter.getGroupCount(); i++) {
            View groupItem = listAdapter.getGroupView(i, false, null, listView);
            groupItem.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);

            totalHeight += groupItem.getMeasuredHeight();

            if (((listView.isGroupExpanded(i)) && (i != group))
                    || ((!listView.isGroupExpanded(i)) && (i == group))) {
                for (int j = 0; j < listAdapter.getChildrenCount(i); j++) {
                    View listItem = listAdapter.getChildView(i, j, false, null,
                            listView);
                    listItem.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);

                    totalHeight += listItem.getMeasuredHeight();

                }
                //Add Divider Height
                totalHeight += listView.getDividerHeight() * (listAdapter.getChildrenCount(i) - 1);
            }
        }
        //Add Divider Height
        totalHeight += listView.getDividerHeight() * (listAdapter.getGroupCount() - 1);

        ViewGroup.LayoutParams params = listView.getLayoutParams();
        int height = totalHeight
                + (listView.getDividerHeight() * (listAdapter.getGroupCount() - 1));
        if (height < 10)
            height = 200;
        params.height = height;
        listView.setLayoutParams(params);
        listView.requestLayout();
    }

    private void testArrayData() {

        headerArray.add("Listening");
        headerArray.add("Academic Reading");
        headerArray.add("General Reading");
        headerArray.add("Academic writing");
        headerArray.add("General writing");
        headerArray.add("Speaking");


        descriArray.add("<p><strong>Timing :&nbsp;&nbsp;</strong>Audio of maximum 30 minutes is played + transfer time of 10 minutes is given.</p>\n" +
                "<p>Note: Test takers write their answers on the question paper as they listen, and at the end of the test are given 10 minutes to transfer their answers to an answer sheet. Care ought to be taken when writing answers on the answer sheet. Poor spelling and grammar are penalised under this section.</p>\n" +
                "<p><strong>Format :&nbsp;&nbsp;</strong>The first 2 components manage things set in everyday social contexts</p>\n" +
                "<p>Part 1: there&rsquo;s a speech communication between 2 speakers (for example, a speech communication concerning travel arrangements)</p>\n" +
                "<p>Part 2: there is a monologue (for example, a speech about local facilities)The final two pieces deal with circumstances that are set in a sense of education and training.</p>\n" +
                "<p>Part 3: there is a conversation between two key speakers (for example, two university students in discussion, perhaps guided by a tutor)</p>\n" +
                "<p>Part 4: There is a scholarly subject monologue there.</p>\n" +
                "<p><strong>Types of Questions :</strong>Fill in the blanks, maps/ plan/ diagram labelling, multiple-choice questions, matching form/note/table/flow-chart/summary completion, sentence completion.</p>\n" +
                "<p><strong>Scores :&nbsp;</strong>Each correct answer is worth 1 mark.</p>");

        descriArray.add("<p>There are three long passages taken from journals, magazines, and books. The passages taken in this test are meant for those who want to go abroad for higher studies.</p>\n" +
                "<p><strong>Timing :&nbsp;&nbsp;</strong>60 minutes</p>\n" +
                "<p>Note: Test takers are required to transfer their answers to an answer sheet during the time allowed for the test. No extra time is allowed for transfer. Care ought to be taken when writing answers on the answer sheet. Poor spelling and grammar are penalised under this section.</p>\n" +
                "<p><strong>Format :&nbsp;</strong>Three reading passages with a variety of questions using several task types.</p>\n" +
                "<p><strong>Number of Questions :&nbsp;</strong>40 questions</p>\n" +
                "<p><strong>Types of Questions :&nbsp;</strong>multiple-choice, identifying information, identifying the writer&rsquo;s views/claims, matching information, matching headings, matching features, matching sentence endings, sentence completion, summary completion, note completion, table completion, flow-chart completion, diagram label completion and short-answer questions.</p>\n" +
                "<p><strong>Scores :&nbsp;</strong>Each correct answer is worth 1 mark.</p>");

        descriArray.add("<p>There are three sections taken from books, magazines, newspapers, notices, advertisements, company handbooks, and guidelines. These are materials you are likely to encounter daily in an English-speaking environment.</p>\n" +
                "<p><strong>Timing :&nbsp;&nbsp;</strong>60 minutes</p>\n" +
                "<p>Note: Test takers are required to transfer their answers to an answer sheet during the time allowed for the test. No extra time is allowed for transfer. Care ought to be taken when writing answers on the answer sheet. Poor spelling and grammar are penalised under this section.</p>\n" +
                "<p><strong>Format :&nbsp;</strong>There are three sections. Section 1 may contain two or three brief texts or several shorter texts. Section 2 comprises two texts. There is one lengthy text in Section 3.</p>\n" +
                "<p><strong>Number of Questions :&nbsp;</strong>40 questions</p>\n" +
                "<p><strong>Types of Questions :&nbsp;</strong>Multiple-choice, identifying information, identifying the writer&rsquo;s views/claims, matching information, matching headings, matching features, matching sentence endings, sentence completion, summary completion, note completion, table completion, flow-chart completion, diagram label completion and short-answer questions.</p>\n" +
                "<p><strong>Scores :&nbsp;</strong>Each correct answer is worth 1 mark.</p>");

        descriArray.add("<p><strong>Timing :&nbsp;&nbsp;</strong>60 minutes</p>\n" +
                "<p>Note: Test takers are required to write their tasks on the answer sheets during the time allowed for the test. There has to be a formal style to respond to both tasks.</p>\n" +
                "<p><strong>Format :&nbsp;</strong>Task -1: Candidates will be given some visual information that will have to be described in at least 150 words in about 20 minutes.&nbsp;</p>\n" +
                "<p>Task -2 &ndash; Candidates respond to a point of view or argument or problem in at least 250 words in about 40 minutes.</p>\n" +
                "<p><strong>Number of Questions :&nbsp;</strong>2 Questions</p>\n" +
                "<p><strong>Types of Questions :&nbsp;</strong>Task -1-graph, table, chart or diagram</p>\n" +
                "<p>Task -2- You will be asked to compose an essay in response to opinions, claims or problems.</p>\n" +
                "<p><strong>Scores :&nbsp;</strong>Responses are assessed by certified IELTS examiners. The points for Task 2 are twice that of Task1&rsquo;s points.</p>");

        descriArray.add("<p><strong>Timing :&nbsp;&nbsp;</strong>60 minutes</p>\n" +
                "<p>Note: Test takers are required to write their tasks in the answer sheets during the time allowed for the test. Responses to task-2 must be in a formal style. While task 2&rsquo;s response could be formal, semi-formal, or informal, it depends on what kind of question is asked.</p>\n" +
                "<p><strong>Format :&nbsp;</strong>Task -1: test takers are asked to respond to a situation by writing in at least 50 words in about 20 minutes.&nbsp;</p>\n" +
                "<p>Task -2 &ndash; Candidates respond to a point of view or argument or problem in at least 250 words in about 40 minutes.</p>\n" +
                "<p><strong>Number of Questions :&nbsp;</strong>2 Questions</p>\n" +
                "<p><strong>Types of Questions :&nbsp;</strong>Task -1- a letter requesting information or explaining a situation.</p>\n" +
                "<p>Task -2- You will be asked to compose an essay in response to opinions, claims or problems.</p>\n" +
                "<p><strong>Scores :&nbsp;</strong>Responses are assessed by certified IELTS examiners. The points for Task 2 are twice that of Task1&rsquo;s points.</p>");

        descriArray.add("<p>This module of IELTS is divided into 3 parts. In this test candidates spoken English is assessed. And every test is recorded.</p>\n" +
                "<p><strong>Timing :&nbsp;</strong>11-14 minutes</p>\n" +
                "<p>Note: Test takers write their answers on the question paper as they listen, and at the end of the test are given 10 minutes to transfer their answers to an answer sheet. Care ought to be taken when writing answers on the answer sheet. Poor spelling and grammar are penalised under this section.</p>\n" +
                "<p><strong>Format/ Timing :&nbsp;</strong>Part-1- In part one the examiner introduces him/herself and asks the candidate general questions on some familiar topics, such as work, hobbies, religion, hometown, and music. This part lasts for 4-5 minutes.</p>\n" +
                "<p>Part-2 &ndash; In this part the candidate is given a card in which there is a topic along with three sub-points to speak on. The candidate is given 1 minute to think and make points if he/she wishes to, and then 2 minutes are given to speak on the topic.</p>\n" +
                "<p>Part-3- In this last part of the speaking test, the examiner discusses the cue card topic by asking some questions which a candidate should spoke elaborately.&nbsp; This part lasts for 4 to 5 minutes.</p>\n" +
                "<p><strong>Number of Questions :&nbsp;</strong>Variable</p>\n" +
                "<p><strong>Scores :&nbsp;</strong>Each correct answer is worth 1 mark.</p>");


    }

}