package com.mystudycanada.shreehari.ui;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.bumptech.glide.Glide;
import com.google.android.material.tabs.TabLayout;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.mystudycanada.shreehari.Chat.Fragments.ChatsFragment;
import com.mystudycanada.shreehari.Chat.Fragments.ProfileFragment;
import com.mystudycanada.shreehari.Chat.Fragments.UsersFragment;
import com.mystudycanada.shreehari.Chat.MessageActivity;
import com.mystudycanada.shreehari.Chat.Model.Chat;
import com.mystudycanada.shreehari.Chat.Model.User;
import com.mystudycanada.shreehari.R;
import com.mystudycanada.shreehari.UserSession.UserSession;

import java.util.ArrayList;
import java.util.HashMap;

import de.hdodenhof.circleimageview.CircleImageView;

public class Connect extends Fragment {
	// Store instance variables


	FirebaseUser firebaseUser;
	DatabaseReference reference;
	private UserSession userSession;


	// Store instance variables based on arguments passed
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);


	}

	// Inflate the view for the fragment based on layout XML
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
							 Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_connect, container, false);



		userSession = new UserSession(getActivity());


		firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
		reference = FirebaseDatabase.getInstance().getReference(userSession.getBranchId()).child("Users").child(firebaseUser.getUid());

		reference.addValueEventListener(new ValueEventListener() {
			@Override
			public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
				User user = dataSnapshot.getValue(User.class);
			//	username.setText(user.getUsername());
				if (user.getImageURL().equals("default")){
					//profile_image.setImageResource(R.mipmap.ic_launcher);
				} else {

					//change this
				//	Glide.with(getActivity()).load(user.getImageURL()).into(profile_image);
				}
			}

			@Override
			public void onCancelled(@NonNull DatabaseError databaseError) {

			}
		});

		final TabLayout tabLayout =  view.findViewById(R.id.tab_layout);
		final ViewPager viewPager =  view.findViewById(R.id.view_pager);


		reference = FirebaseDatabase.getInstance().getReference(userSession.getBranchId()).child("Chats").child(userSession.getBranchId()+"-"+userSession.getUserId());
		reference.addValueEventListener(new ValueEventListener() {
			@Override
			public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
				ViewPagerAdapter viewPagerAdapter = new ViewPagerAdapter(getActivity().getSupportFragmentManager());
				int unread = 0;
				for (DataSnapshot snapshot : dataSnapshot.getChildren()){
					Chat chat = snapshot.getValue(Chat.class);
					if (chat.getReceiver().equals(firebaseUser.getUid()) && !chat.isIsseen()){
						unread++;
					}
				}



				viewPagerAdapter.addFragment(new UsersFragment(), "Users");

				if (unread == 0){
					viewPagerAdapter.addFragment(new ChatsFragment(), "Chats");
				} else {
					viewPagerAdapter.addFragment(new ChatsFragment(), "("+unread+") Chats");
				}
			//	viewPagerAdapter.addFragment(new ProfileFragment(), "Profile");

				viewPager.setAdapter(viewPagerAdapter);

				tabLayout.setupWithViewPager(viewPager);

			}

			@Override
			public void onCancelled(@NonNull DatabaseError databaseError) {

			}
		});


		return view;
	}


	class ViewPagerAdapter extends FragmentPagerAdapter {

		private ArrayList<Fragment> fragments;
		private ArrayList<String> titles;

		ViewPagerAdapter(FragmentManager fm){
			super(fm);
			this.fragments = new ArrayList<>();
			this.titles = new ArrayList<>();
		}

		@Override
		public Fragment getItem(int position) {
			return fragments.get(position);
		}

		@Override
		public int getCount() {
			return fragments.size();
		}

		public void addFragment(Fragment fragment, String title){
			fragments.add(fragment);
			titles.add(title);
		}

		// Ctrl + O

		@Nullable
		@Override
		public CharSequence getPageTitle(int position) {
			return titles.get(position);
		}
	}




}