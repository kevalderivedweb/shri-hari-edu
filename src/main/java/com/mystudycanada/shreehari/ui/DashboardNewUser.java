package com.mystudycanada.shreehari.ui;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.DatePicker;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.IdRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.mystudycanada.shreehari.API.GetBatchRequest;
import com.mystudycanada.shreehari.API.GetCoachingLevelRequest;
import com.mystudycanada.shreehari.API.GetDemoLecture;
import com.mystudycanada.shreehari.API.GetRegisterWithUs;
import com.mystudycanada.shreehari.API.GetStandardRequest;
import com.mystudycanada.shreehari.Adapter.CoatchingLevelAdapter;
import com.mystudycanada.shreehari.Adapter.SpinAdapter;
import com.mystudycanada.shreehari.Adapter.SpinAdapter2;
import com.mystudycanada.shreehari.HomeActivity;
import com.mystudycanada.shreehari.Login_Activity;
import com.mystudycanada.shreehari.Model.BatchModel;
import com.mystudycanada.shreehari.Model.CoachingLevelModel;
import com.mystudycanada.shreehari.Model.SubjectModel;
import com.mystudycanada.shreehari.R;
import com.mystudycanada.shreehari.UserSession.UserSession;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.BreakIterator;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

public class DashboardNewUser extends Fragment {

    private UserSession session;



    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_dashboard_new_user, container, false);

        session = new UserSession(getActivity());


        view.findViewById(R.id.moreInfo).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                replaceFragment(R.id.nav_host_fragment, new MoreInfoIELTS(), "FragmentMoreInfo", null);

            }
        });


        view.findViewById(R.id.aboutFrag).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                replaceFragment(R.id.nav_host_fragment, new AboutIelts(), "FragmentAboutIelts", null);

            }
        });

        view.findViewById(R.id.canadaNew4User).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                replaceFragment(R.id.nav_host_fragment, new NewUserCanada("canada"), "FragmentCanadaNewUser", null);

            }
        });

        view.findViewById(R.id.ausNew4User).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                replaceFragment(R.id.nav_host_fragment, new NewUserCanada("aus"), "FragmentAusNewUser", null);

            }
        });

        view.findViewById(R.id.UKNew4User).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                replaceFragment(R.id.nav_host_fragment, new NewUserCanada("UK"), "FragmentUKNewUser", null);

            }
        });

        view.findViewById(R.id.USNew4User).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                replaceFragment(R.id.nav_host_fragment, new NewUserCanada("US"), "FragmentUSNewUser", null);

            }
        });

        view.findViewById(R.id.newZeaNew4User).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                replaceFragment(R.id.nav_host_fragment, new NewUserCanada("newZealand"), "FragmentNewZealandNewUser", null);

            }
        });
        view.findViewById(R.id.company_profile).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                WebviewFragment fragobj = new WebviewFragment();
                Bundle bundle = new Bundle();
               // bundle.putString("Web_Url", "https://shreehari.in/company-profile.pdf");
                bundle.putString("Web_Url", "https://drive.google.com/viewerng/viewer?embedded=true&url=https://shreehari.in/company-profile.pdf");
                fragobj.setArguments(bundle);
                replaceFragment(R.id.nav_host_fragment,fragobj,"Fragment",null);

            }
        });

        return view;
    }




    protected void replaceFragment(@IdRes int containerViewId,
                                   @NonNull Fragment fragment,
                                   @NonNull String fragmentTag,
                                   @Nullable String backStackStateName) {
        getActivity().getSupportFragmentManager()
                .beginTransaction()
                .replace(containerViewId, fragment, fragmentTag)
                .addToBackStack(backStackStateName)
                .commit();

    }

}
