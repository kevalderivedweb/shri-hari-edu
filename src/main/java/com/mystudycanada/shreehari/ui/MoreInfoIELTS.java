package com.mystudycanada.shreehari.ui;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.IdRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.google.gson.JsonObject;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.mystudycanada.shreehari.API.GetBlogList;
import com.mystudycanada.shreehari.Adapter.MoreInfoIELTSListAdapter;
import com.mystudycanada.shreehari.Adapter.SpinAdapter;
import com.mystudycanada.shreehari.Model.BatchModel;
import com.mystudycanada.shreehari.Model.MoreInfoIeltsModel;
import com.mystudycanada.shreehari.R;
import com.mystudycanada.shreehari.UserSession.UserSession;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class MoreInfoIELTS extends Fragment {

    private ArrayList<MoreInfoIeltsModel> ieltsModelArrayList = new ArrayList<>();
    private MoreInfoIELTSListAdapter moreInfoIELTSListAdapter;

    private RecyclerView recMoreInfoList;

    private RequestQueue requestQueue;
    private UserSession session;


    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_more_info_ielts, container, false);

        requestQueue = Volley.newRequestQueue(getActivity());//Creating the RequestQueue
        session = new UserSession(getActivity());

        recMoreInfoList = view.findViewById(R.id.recMoreInfoList);

        recMoreInfoList.setLayoutManager(new GridLayoutManager(getContext(), 2));
        moreInfoIELTSListAdapter = new MoreInfoIELTSListAdapter(getContext(), ieltsModelArrayList, new MoreInfoIELTSListAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(String id, String title) {

                replaceFragment(R.id.nav_host_fragment, new BlogList(id, title), "FragmentMoreInfo", null);

            }
        });
        recMoreInfoList.setAdapter(moreInfoIELTSListAdapter);


        view.findViewById(R.id.back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //   getFragmentManager().popBackStack();

                FragmentManager fm = getFragmentManager();
                if (fm.getBackStackEntryCount() > 0) {
                    Log.i("MainActivity", "popping backstack");
                    fm.popBackStack();
                } else {
                    Log.i("MainActivity", "nothing on backstack, calling super");
                }

            }
        });

        GetBlogList();

        return view;

    }


    private void GetBlogList() {

        final KProgressHUD progressDialog = KProgressHUD.create(getActivity())
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("Please wait")
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();

        GetBlogList getBlogList = new GetBlogList(new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("Response", response + " null");
                ieltsModelArrayList.clear();
                progressDialog.dismiss();

                JSONObject jsonObject = null;
                try {
                    jsonObject = new JSONObject(response);

                    if (jsonObject.getString("ResponseCode").equals("200")){

                        JSONArray jsonArray = jsonObject.getJSONArray("data");

                        for (int i = 0; i < jsonArray.length(); i++){
                            JSONObject jsonObject1 = jsonArray.getJSONObject(i);

                            MoreInfoIeltsModel moreInfoIeltsModel = new MoreInfoIeltsModel();
                            moreInfoIeltsModel.setId(jsonObject1.getString("blog_category_id"));
                            moreInfoIeltsModel.setImage(jsonObject1.getString("icon"));
                            moreInfoIeltsModel.setTitle(jsonObject1.getString("blog_category"));

                            ieltsModelArrayList.add(moreInfoIeltsModel);

                        }


                        moreInfoIELTSListAdapter.notifyDataSetChanged();


                    }



                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                progressDialog.dismiss();
                if (error instanceof ServerError)
                    Toast.makeText(getActivity(), "Server Error", Toast.LENGTH_LONG).show();
                else if (error instanceof TimeoutError)
                    Toast.makeText(getActivity(), "Connection Timed Out", Toast.LENGTH_LONG).show();
                else if (error instanceof NetworkError)
                    Toast.makeText(getActivity(), "Bad Network Connection", Toast.LENGTH_LONG).show();
            }
        }){@Override
        public Map<String, String> getHeaders() throws AuthFailureError {
            Map<String, String> params = new HashMap<String, String>();
// params.put("Accept", "application/json");
            params.put("Authorization","Bearer "+ session.getAPIToken());
            return params;
        }};
        getBlogList.setTag("TAG");
        getBlogList.setShouldCache(false);
        getBlogList.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 60000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 60000;
            }

            @Override
            public void retry(VolleyError error) throws VolleyError {

            }
        });
        requestQueue.add(getBlogList);



    }




    protected void replaceFragment(@IdRes int containerViewId,
                                   @NonNull Fragment fragment,
                                   @NonNull String fragmentTag,
                                   @Nullable String backStackStateName) {
        getActivity().getSupportFragmentManager()
                .beginTransaction()
                .replace(containerViewId, fragment, fragmentTag)
                .addToBackStack(backStackStateName)
                .commit();

    }

}
