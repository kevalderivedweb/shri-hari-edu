package com.mystudycanada.shreehari.ui;

import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import com.mystudycanada.shreehari.R;

public class WhoConductIELTS extends Fragment {

    private TextView hisText, camText;

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_who_conduct_ielts, container, false);

        hisText = view.findViewById(R.id.hisText);
        camText = view.findViewById(R.id.camText);

        view.findViewById(R.id.back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //   getFragmentManager().popBackStack();

                FragmentManager fm = getFragmentManager();
                if (fm.getBackStackEntryCount() > 0) {
                    Log.i("MainActivity", "popping backstack");
                    fm.popBackStack();
                } else {
                    Log.i("MainActivity", "nothing on backstack, calling super");
                }

            }
        });

        hisText.setText(Html.fromHtml("<p>IELTS is an international test of English language proficiency for non-native English language speakers. This test is jointly managed by <strong>Cambridge Assessment English and IDP: IELTS Australia</strong></p>\n" +
                "<p>From July 2021, IDP: IELTS Australia and Cambridge Assessment English will solely conduct the IELTS test in India.</p>\n" +
                "<p>All tests which are conducted by IDP are compiled by Cambridge English Language Assessment.</p>"));

        camText.setText(Html.fromHtml("<p><strong>Cambridge Assessment English:</strong> Cambridge Assessment English, or University of Cambridge English Language Assessment, is a non-teaching department of the University of Cambridge.</p>\n" +
                "<p>Cambridge Assessment English is the producer of IELTS. IELTS tools and materials are provided by this agency to help the learners plan and practice their exams.</p>"));

        return view;
    }


}

