package com.mystudycanada.shreehari.Chat.Fragments;


import com.mystudycanada.shreehari.Chat.Notifications.MyResponse;
import com.mystudycanada.shreehari.Chat.Notifications.Sender;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public interface APIService {
    @Headers(
            {
                    "Content-Type:application/json",
                    "Authorization:key=AAAAZuvfUe4:APA91bE2gWr7TJB2zmgUpoAZrniEwwoYhj9AbKkD21KGU45zPWOgXPT1vbyxj4KYpWNdtnVlnPoEpEw78Ilg1yP07U3DSbIaqExnBsiNC91_sFfGnJ84Uf9SdRSp9D0xFQTtoz3Q0r59"
            }
    )
    @POST("fcm/send")
    Call<MyResponse> sendNotification(@Body Sender body);
}
